package tech.firas.framework.validator;

import java.util.UUID;
import javax.validation.ValidationException;

import tech.firas.framework.po.FieldOfBean;
import tech.firas.framework.po.ValidationRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import tech.firas.framework.pojo.DataType;
import tech.firas.util.BasicRandomUtils;
import tech.firas.util.RandomStringUtils;

public class ValidatorTests {

    @Test
    public void testNotNull() throws Exception {
        final FieldOfBean field = new FieldOfBean();
        field.setName("name");
        field.setDataType(DataType.VARCHAR);

        final ValidationRule validationRule = new ValidationRule();
        validationRule.setValidationType(ValidationRule.TYPE_NOT_NULL);
        validationRule.setField(field);
        validationRule.setErrorMessage("notNull.name.test");

        for (int i = 1000; i > 0; i -= 1) {
            Validator.validate(validationRule, RandomStringUtils.getDefault().randomAsciiString(0, 20));
        }
        try {
            Validator.validate(validationRule, null);
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }
    }

    @Test
    public void testMin() throws Exception {
        final FieldOfBean field = new FieldOfBean();
        field.setName("name");
        field.setDataType(DataType.VARCHAR);

        final ValidationRule validationRule = new ValidationRule();
        validationRule.setValidationType(ValidationRule.TYPE_MIN);
        validationRule.setField(field);
        validationRule.setErrorMessage("min(1).name.test");
        validationRule.setValidationValue("1");

        Validator.validate(validationRule, null);
        for (int i = 1000; i > 0; i -= 1) {
            Validator.validate(validationRule, RandomStringUtils.getDefault().randomAsciiString(1, 20));
        }
        try {
            Validator.validate(validationRule, "");
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }

        field.setName("precisionOrLength");
        field.setDataType(DataType.INT32);

        validationRule.setErrorMessage("minInt(1).precisionOrLength.test");

        Validator.validate(validationRule, null);
        for (int i = 1000; i > 0; i -= 1) {
            Validator.validate(validationRule, Integer.toString(BasicRandomUtils.getDefault().randomInt(1, Integer.MAX_VALUE - 1)));
        }
        try {
            Validator.validate(validationRule, "0");
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }
        testValidatingInteger(validationRule);
    }

    @Test
    public void testMax() throws Exception {
        final FieldOfBean field = new FieldOfBean();
        field.setName("name");
        field.setDataType(DataType.VARCHAR);

        final ValidationRule validationRule = new ValidationRule();
        validationRule.setValidationType(ValidationRule.TYPE_MAX);
        validationRule.setField(field);
        validationRule.setErrorMessage("max(1).name.test");
        validationRule.setValidationValue("2");

        Validator.validate(validationRule, null);
        Validator.validate(validationRule, "");
        Validator.validate(validationRule, "12");
        try {
            Validator.validate(validationRule, "123");
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }

        field.setName("dataType");
        field.setDataType(DataType.INT32);

        validationRule.setValidationValue("4");
        validationRule.setErrorMessage("maxInt(4).dataType.test");

        Validator.validate(validationRule, null);
        Validator.validate(validationRule, "0");
        Validator.validate(validationRule, "4");
        try {
            Validator.validate(validationRule, "5");
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }
        testValidatingInteger(validationRule);
    }

    private static void testValidatingInteger(final ValidationRule validationRule) {
        try {
            Validator.validate(validationRule, "1.5");
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }
        try {
            Validator.validate(validationRule, "");
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }
        try {
            Validator.validate(validationRule, " ");
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }
    }

    @Test
    public void testPattern() {
        final FieldOfBean field = new FieldOfBean();
        field.setName("id");
        field.setDataType(DataType.VARCHAR);

        final ValidationRule validationRule = new ValidationRule();
        validationRule.setValidationType(ValidationRule.TYPE_MATCH);
        validationRule.setValidationValue("^[A-Fa-f\\d]{8}(-[A-Fa-f\\d]{4}){3}-[A-Fa-f\\d]{12}$");
        validationRule.setErrorMessage("id pattern.test");
        validationRule.setField(field);

        Validator.validate(validationRule, null);
        for (int i = 1000; i > 0; i -= 1) {
            Validator.validate(validationRule, UUID.randomUUID().toString());
        }
        try {
            Validator.validate(validationRule, RandomStringUtils.getDefault().randomAsciiString(1, 35));
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }
        try {
            Validator.validate(validationRule, RandomStringUtils.getDefault().randomAsciiString(37, 100));
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }

        field.setName("name");

        validationRule.setValidationType(ValidationRule.TYPE_NOT_MATCH);
        validationRule.setValidationValue("^\\d*$");
        validationRule.setErrorMessage("name pattern test");

        Validator.validate(validationRule, null);
        for (int i = 1000; i > 0; i -= 1) {
            Validator.validate(validationRule, RandomStringUtils.getDefault().randomAlphabeticString(1, 100));
        }
        try {
            Validator.validate(validationRule, RandomStringUtils.getDefault().randomNumericString(0, 100));
            Assertions.fail("Expected to throw a ValidationException");
        } catch (ValidationException ex) {
            Assertions.assertEquals(validationRule.getErrorMessage(), ex.getMessage());
        }
    }
}
