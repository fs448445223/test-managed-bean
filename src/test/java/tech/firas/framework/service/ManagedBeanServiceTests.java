package tech.firas.framework.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.validation.ValidationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import tech.firas.framework.Application;
import tech.firas.framework.po.FieldOfBean;
import tech.firas.framework.po.ManagedBean;
import tech.firas.framework.po.StatusModel;
import tech.firas.framework.pojo.DataType;
import tech.firas.framework.service.ManagedBeanService;

@SpringBootTest(classes = {Application.class})
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@WithMockUser(username = "superadmin", authorities = {"OWNER:00000000-0000-0000-0000-000000000000",
        "EDITOR:00000000-0000-0000-0000-000000000001"})
public class ManagedBeanServiceTests {

    private ManagedBeanService managedBeanService;

    @Inject
    public void setManagedBeanService(final ManagedBeanService managedBeanService) {
        this.managedBeanService = managedBeanService;
    }

    @Test
    public void testGetById() {
        Assertions.assertFalse(this.managedBeanService.getById(
                UUID.fromString("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF")).isPresent());

        final UUID id1 = UUID.fromString("00000000-0000-0000-0000-000000000000");
        final Optional<ManagedBean> result1 = this.managedBeanService.getById(id1);
        Assertions.assertTrue(result1.isPresent());
        Assertions.assertEquals(id1, result1.get().getId());
        Assertions.assertNotNull(result1.get().getFields());
        Assertions.assertEquals(7, result1.get().getFields().size());

        final UUID id2 = UUID.fromString("00000000-0000-0000-0000-000000000001");
        final Optional<ManagedBean> result2 = this.managedBeanService.getById(id2);
        Assertions.assertTrue(result2.isPresent());
        Assertions.assertEquals(id2, result2.get().getId());
        Assertions.assertNotNull(result2.get().getFields());
        Assertions.assertEquals(9, result2.get().getFields().size());
    }

    @Test
    public void testGetByName() {
        Assertions.assertFalse(this.managedBeanService.getByName("Not exist").isPresent());

        final String name1 = "managedBean";
        final Optional<ManagedBean> result1 = this.managedBeanService.getByName(name1);
        Assertions.assertTrue(result1.isPresent());
        Assertions.assertEquals(name1, result1.get().getName());

        final String name2 = "fieldOfBean";
        final Optional<ManagedBean> result2 = this.managedBeanService.getByName(name2);
        Assertions.assertTrue(result2.isPresent());
        Assertions.assertEquals(name2, result2.get().getName());
    }

    @Test
    public void testInsertOrUpdate() throws Exception {
        final ManagedBean inserted = this.managedBeanService.insertOrUpdate(getForInsert());
        Assertions.assertNotNull(inserted);
        Assertions.assertNotNull(inserted.getId());
        Assertions.assertEquals("testTest", inserted.getName());
        Assertions.assertEquals("test_test", inserted.getDbTable());

        Assertions.assertNotNull(inserted.getFields());
        Assertions.assertEquals(5, inserted.getFields().size());
        assertionsOfFields(inserted);

        final ManagedBean updated = this.managedBeanService.insertOrUpdate(getForUpdate(inserted));
        Assertions.assertNotNull(inserted);
        Assertions.assertEquals(inserted.getId(), updated.getId());
        Assertions.assertEquals("testTest", updated.getName());
        Assertions.assertEquals("t_test_test", updated.getDbTable());

        Assertions.assertNotNull(updated.getFields());
        Assertions.assertEquals(7, updated.getFields().size());
        assertionsOfFields(updated);

        updated.setName("0");
        try {
            this.managedBeanService.insertOrUpdate(updated);
            Assertions.fail("Expected to throw a ValidationException");
        } catch (final ValidationException ex) {
            Assertions.assertNotNull(ex.getMessage());
            Assertions.assertFalse(ex.getMessage().trim().isEmpty());
        }
    }

    @Test
    @Transactional(readOnly = true)
    public void testGetBeanById() throws Exception {
        final String idStr = "00000000-0000-0000-0000-000000000000";
        final UUID id = UUID.fromString(idStr); // id of managedBean
        final Optional<ManagedBean> managedBean = this.managedBeanService.getByName("managedBean");
        Assertions.assertTrue(managedBean.isPresent());
        Assertions.assertNotNull(managedBean.get().getFields());
        final Map<String, Object> bean = this.managedBeanService.getBeanById(managedBean.get(), id);
        Assertions.assertNotNull(bean);
        Assertions.assertNotNull(bean.get("id"));
        Assertions.assertEquals(idStr, bean.get("id").toString());
        Assertions.assertTrue(bean.get("name") instanceof String);
        Assertions.assertFalse(bean.get("name").toString().isEmpty());
        Assertions.assertTrue(bean.get("dbTable") instanceof String);
        Assertions.assertFalse(bean.get("dbTable").toString().isEmpty());
    }

    @Test
    @Transactional(readOnly = true)
    public void testListBeanByPage() throws Exception {
        final Optional<ManagedBean> optional = this.managedBeanService.getByName("fieldOfBean");
        Assertions.assertTrue(optional.isPresent());
        final ManagedBean managedBean = optional.get();
        final Page<Map<String, Object>> page0 = this.managedBeanService.listBeanByPage(
                managedBean, PageRequest.of(0, 5));
        Assertions.assertNotNull(page0);
        Assertions.assertEquals(0, page0.getNumber());
        Assertions.assertEquals(5, page0.getSize());
        Assertions.assertEquals(5, page0.getNumberOfElements());
        Assertions.assertTrue(page0.getTotalElements() > 5);
        for (final Map<String, Object> fieldBean : page0) {
            assertionsOfFieldBean(fieldBean);
        }

        final Page<Map<String, Object>> page1 = this.managedBeanService.listBeanByPage(
                managedBean, PageRequest.of(1, 5));
        Assertions.assertNotNull(page1);
        Assertions.assertEquals(1, page1.getNumber());
        Assertions.assertEquals(5, page1.getSize());
        Assertions.assertEquals(5, page1.getNumberOfElements());
        Assertions.assertTrue(page1.getTotalElements() > 5);
        for (final Map<String, Object> fieldBean : page1) {
            assertionsOfFieldBean(fieldBean);
        }
    }

    @Test
    public void testInsertOrUpdateBean() throws Exception {
        final Optional<ManagedBean> optional = this.managedBeanService.getByName("fieldOfBean");
        Assertions.assertTrue(optional.isPresent());
        final ManagedBean managedBean = optional.get();
        final String beanId = "00000000-0000-0000-0000-000000000001";
        final Map<String, Object> toInsert = new HashMap<>();
        final String name = "datetimePattern", columnName = "date_time_pattern";
        final int length = 40;
        toInsert.put("beanId", beanId);
        toInsert.put("status", StatusModel.STATUS_NORMAL);
        toInsert.put("name", name);
        toInsert.put("dbColumn", columnName);
        toInsert.put("dataType", DataType.VARCHAR.ordinal());
        toInsert.put("precisionOrLength", length);

        final Map<String, Object> inserted = this.managedBeanService.insertOrUpdateBean(managedBean, toInsert);
        Assertions.assertNotNull(inserted);
        final Object id = inserted.get("id");
        Assertions.assertTrue(id instanceof String || id instanceof UUID);
        final Object createTimestamp = inserted.get("createTimestamp");
        Assertions.assertTrue(createTimestamp instanceof Long);
        Assertions.assertEquals(beanId, inserted.get("beanId"));
        Assertions.assertEquals(name, inserted.get("name"));
        Assertions.assertEquals(columnName, inserted.get("dbColumn"));
        Assertions.assertEquals(DataType.VARCHAR.ordinal(), inserted.get("dataType"));
        Assertions.assertEquals(length, inserted.get("precisionOrLength"));

        final Map<String, Object> toUpdate = new HashMap<>(inserted);
        final String newName = "dateTimePattern";
        toUpdate.put("name", newName);

        final Map<String, Object> updated = this.managedBeanService.insertOrUpdateBean(managedBean, toUpdate);
        Assertions.assertNotNull(updated);
        Assertions.assertNotNull(updated.get("id"));
        Assertions.assertEquals(id.toString(), updated.get("id").toString());
        final Object updateTimestamp = inserted.get("updateTimestamp");
        Assertions.assertTrue(updateTimestamp instanceof Long);
        Assertions.assertTrue( ((Long) updateTimestamp).compareTo((Long) createTimestamp) >= 0 );
        Assertions.assertEquals(newName, updated.get("name"));
        Assertions.assertEquals(columnName, updated.get("dbColumn"));
        Assertions.assertEquals(DataType.VARCHAR.ordinal(), updated.get("dataType"));
        Assertions.assertEquals(length, updated.get("precisionOrLength"));
    }

    private void assertionsOfFieldBean(final Map<String, Object> fieldBean) {
        Assertions.assertNotNull(fieldBean);
        Assertions.assertNotNull(fieldBean.get("id"));
        Assertions.assertFalse(fieldBean.get("id").toString().isEmpty());
        Assertions.assertTrue(fieldBean.get("name") instanceof String);
        Assertions.assertFalse(fieldBean.get("name").toString().isEmpty());
        Assertions.assertTrue(fieldBean.get("dbColumn") instanceof String);
        Assertions.assertFalse(fieldBean.get("dbColumn").toString().isEmpty());
    }

    private void assertionsOfFields(final ManagedBean managedBean) {
        for (final FieldOfBean field : managedBean.getFields()) {
            Assertions.assertNotNull(field);
            Assertions.assertNotNull(field.getId());
            Assertions.assertNotNull(field.getName());
            Assertions.assertFalse(field.getName().isEmpty());
            Assertions.assertNotNull(field.getDbColumn());
            Assertions.assertFalse(field.getDbColumn().isEmpty());
            Assertions.assertEquals(managedBean, field.getBean());
        }
    }

    private ManagedBean getForInsert() {
        final FieldOfBean idField = new FieldOfBean();
        idField.setName("id");
        idField.setDbColumn("id");
        idField.setDataType(DataType.UUID);

        final FieldOfBean fieldIdField = new FieldOfBean();
        fieldIdField.setName("fieldId");
        fieldIdField.setDbColumn("field_id");
        fieldIdField.setDataType(DataType.UUID);

        final FieldOfBean validationTypeField = new FieldOfBean();
        validationTypeField.setName("validationType");
        validationTypeField.setDbColumn("validation_type");
        validationTypeField.setDataType(DataType.INT32);

        final FieldOfBean validationValueField = new FieldOfBean();
        validationValueField.setName("validationValue");
        validationValueField.setDbColumn("validation_value");
        validationValueField.setDataType(DataType.VARCHAR);
        validationValueField.setPrecisionOrLength(2000);

        final FieldOfBean errorMessageField = new FieldOfBean();
        errorMessageField.setName("errorMessage");
        errorMessageField.setDbColumn("error_message");
        errorMessageField.setDataType(DataType.VARCHAR);
        errorMessageField.setPrecisionOrLength(100);

        final ManagedBean managedBean = new ManagedBean();
        managedBean.setName("testTest");
        managedBean.setDbTable("test_test");
        managedBean.setFields(Arrays.asList(
                idField, fieldIdField, validationTypeField, validationValueField, errorMessageField));

        return managedBean;
    }

    private ManagedBean getForUpdate(final ManagedBean managedBean) {
        managedBean.setDbSchema("public");
        managedBean.setDbTable("t_test_test");

        final List<FieldOfBean> fields = managedBean.getFields().stream().peek((field) -> {
            if (field.getDbColumn().equals("validation_type")) {
                field.setDbColumn("valid_type");
            } else if (field.getDbColumn().equals("validation_value")) {
                field.setDbColumn("valid_value");
            }
        }).collect(Collectors.toList());

        final FieldOfBean createTimestampField = new FieldOfBean();
        createTimestampField.setId(UUID.fromString("00000000-0000-0000-0000-000000000002"));
        createTimestampField.setName("createTimestamp");
        createTimestampField.setDbColumn("create_timestamp");
        createTimestampField.setDataType(DataType.INT64);

        final FieldOfBean updateTimestampField = new FieldOfBean();
        updateTimestampField.setName("updateTimestamp");
        updateTimestampField.setDbColumn("update_timestamp");
        updateTimestampField.setDataType(DataType.INT64);

        fields.add(createTimestampField);
        fields.add(updateTimestampField);
        managedBean.setFields(fields);
        return managedBean;
    }
}
