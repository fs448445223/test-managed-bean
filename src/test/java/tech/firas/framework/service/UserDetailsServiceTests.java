package tech.firas.framework.service;

import java.util.Collection;
import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import tech.firas.framework.Application;

@SpringBootTest(classes = {Application.class})
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class UserDetailsServiceTests {

    private UserDetailsService userDetailsService;

    @Inject
    public void setUserDetailsService(final UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Test
    public void testLoadUserByUsername() {
        final UserDetails superAdmin = this.userDetailsService.loadUserByUsername("superadmin");
        final Collection<? extends GrantedAuthority> superAuthorities = superAdmin.getAuthorities();
        Assertions.assertFalse(superAuthorities.isEmpty());

        final UserDetails test1 = this.userDetailsService.loadUserByUsername("test1");
        final Collection<? extends GrantedAuthority> testAuthorities = test1.getAuthorities();
        Assertions.assertFalse(testAuthorities.isEmpty());
    }
}
