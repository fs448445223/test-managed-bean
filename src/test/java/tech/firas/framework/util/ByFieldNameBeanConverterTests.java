package tech.firas.framework.util;

import java.util.Random;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import tech.firas.framework.po.ManagedBeanLog;
import tech.firas.framework.pojo.WebClientInfo;
import tech.firas.util.BasicRandomUtils;
import tech.firas.util.RandomStringUtils;

public class ByFieldNameBeanConverterTests {

    private Random random = new Random();

    @Test
    public void test() throws NoSuchMethodException {
        testConvertWebClientInfo(new ByFieldNameBeanConverter<>(WebClientInfo.class, ManagedBeanLog.class, null));

        final ByFieldNameBeanConverter.Configuration configuration = new ByFieldNameBeanConverter.Configuration();
        configuration.setAllowDirectlyGetField(true);
        configuration.setAllowDirectlySetField(true);
        configuration.setAllowGetBoolean(true);
        testConvertWebClientInfo(new ByFieldNameBeanConverter<>(WebClientInfo.class, ManagedBeanLog.class, null));
    }

    private void testConvertWebClientInfo(final ByFieldNameBeanConverter<WebClientInfo, ManagedBeanLog> converter) {
        for (int i = 0; i < 10; i += 1) {
            final WebClientInfo webClientInfo = new WebClientInfo();
            webClientInfo.setOperatorId(UUID.randomUUID());
            webClientInfo.setOperatorIp(BasicRandomUtils.getDefault().randomInt(0, 255) + "." +
                    BasicRandomUtils.getDefault().randomInt(0, 255) + "." +
                    BasicRandomUtils.getDefault().randomInt(0, 255) + "." +
                    BasicRandomUtils.getDefault().randomInt(0, 255));
            webClientInfo.setUserAgent("Mozilla/5.0 " + RandomStringUtils.getDefault().randomAsciiString(5, 20));

            final ManagedBeanLog managedBeanLog = converter.apply(webClientInfo);
            Assertions.assertNotNull(managedBeanLog);
            Assertions.assertEquals(webClientInfo.getOperatorId(), managedBeanLog.getOperatorId());
            Assertions.assertEquals(webClientInfo.getOperatorIp(), managedBeanLog.getOperatorIp());
            Assertions.assertEquals(webClientInfo.getUserAgent(), managedBeanLog.getUserAgent());
            Assertions.assertNull(managedBeanLog.getId());
        }
    }
}
