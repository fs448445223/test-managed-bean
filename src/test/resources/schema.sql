CREATE TABLE t_managed_bean (
    id UUID,
    create_timestamp BIGINT NOT NULL,
    update_timestamp BIGINT NOT NULL,
    status SMALLINT NOT NULL DEFAULT 0,
    name VARCHAR(40) NOT NULL,
    db_table VARCHAR(40) NOT NULL,
    db_schema VARCHAR(40),
    PRIMARY KEY (id)
);
CREATE TABLE t_managed_bean_log (
    id UUID,
    create_timestamp BIGINT NOT NULL,
    update_timestamp BIGINT NOT NULL,
    status SMALLINT NOT NULL DEFAULT 0,
    content VARCHAR(4000) NOT NULL,
    is_create SMALLINT NOT NULL,
    model_id UUID NOT NULL,
    operator_id UUID,
    operator_ip VARCHAR(40),
    user_agent VARCHAR(400),
    bean_type INT NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE t_field_of_bean (
    id UUID,
    create_timestamp BIGINT NOT NULL,
    update_timestamp BIGINT NOT NULL,
    status SMALLINT NOT NULL DEFAULT 0,
    bean_id UUID NOT NULL,
    name VARCHAR(40) NOT NULL,
    db_column VARCHAR(40) NOT NULL,
    data_type INT NOT NULL,
    precision_or_length INT,
    date_time_pattern VARCHAR(40),
    show_on_table SMALLINT NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX i_field_name ON t_field_of_bean (bean_id, name);

CREATE TABLE t_validation_rule (
    id UUID,
    create_timestamp BIGINT NOT NULL,
    update_timestamp BIGINT NOT NULL,
    status SMALLINT NOT NULL DEFAULT 0,
    field_id UUID NOT NULL,
    validation_type INT NOT NULL,
    validation_value VARCHAR(3000),
    error_message VARCHAR(100),
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX i_validation_rule_field ON t_validation_rule (field_id, validation_type);

CREATE TABLE t_user (
    id UUID,
    create_timestamp BIGINT NOT NULL,
    update_timestamp BIGINT NOT NULL,
    status SMALLINT NOT NULL DEFAULT 0,
    username VARCHAR(40) NOT NULL,
    password VARCHAR(128) NOT NULL,
    nick_name VARCHAR(60),
    gender INT,
    birthday BIGINT,
    avatar_url VARCHAR(512),
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX i_username ON t_user (username);

CREATE TABLE t_role (
    id UUID,
    create_timestamp BIGINT NOT NULL,
    update_timestamp BIGINT NOT NULL,
    status SMALLINT NOT NULL DEFAULT 0,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE t_user_role (
    user_id UUID,
    role_id UUID,
    create_timestamp BIGINT NOT NULL,
    update_timestamp BIGINT NOT NULL,
    status SMALLINT NOT NULL DEFAULT 0,
    PRIMARY KEY (user_id, role_id)
);

CREATE TABLE t_user_permission (
    id UUID,
    create_timestamp BIGINT NOT NULL,
    update_timestamp BIGINT NOT NULL,
    status SMALLINT NOT NULL DEFAULT 0,
    user_id UUID NOT NULL,
    bean_id UUID NOT NULL,
    permission_type INT NOT NULL,
    weight INT NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX i_user_bean_id ON t_user_permission (user_id, bean_id);

CREATE TABLE t_role_permission (
    id UUID,
    create_timestamp BIGINT NOT NULL,
    update_timestamp BIGINT NOT NULL,
    status SMALLINT NOT NULL DEFAULT 0,
    role_id UUID NOT NULL,
    bean_id UUID NOT NULL,
    permission_type INT NOT NULL,
    weight INT NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX i_role_bean_id ON t_role_permission (role_id, bean_id);
