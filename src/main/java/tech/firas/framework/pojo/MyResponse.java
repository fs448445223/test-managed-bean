package tech.firas.framework.pojo;

import java.io.Serializable;

import lombok.Getter;
import com.fasterxml.jackson.annotation.JsonView;
import tech.firas.framework.service.ServiceErrorCode;

@lombok.EqualsAndHashCode
public class MyResponse<T extends Serializable> implements Serializable {

    public MyResponse(final String code, final String msg, final T content) {
        this.code = code;
        this.msg = msg;
        this.content = content;
        this.timestamp = System.currentTimeMillis();
    }

    public MyResponse(final ServiceErrorCode errorCode, final String msg, final T content) {
        this(errorCode.getCode(), msg, content);
    }

    public MyResponse(final String msg, final T content) {
        this("ok", msg, content);
    }

    public MyResponse(final T content) {
        this("ok", content);
    }

    @JsonView(DefaultJsonView.class)
    @Getter private final String code;

    @JsonView(DefaultJsonView.class)
    @Getter private final String msg;

    @JsonView(DefaultJsonView.class)
    @Getter private final T content;

    @JsonView(DefaultJsonView.class)
    @Getter private final long timestamp;
}
