package tech.firas.framework.pojo;

@lombok.AllArgsConstructor
@lombok.Data
public class ClientInfo implements java.io.Serializable {
    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;

    private String ip;
    private String userAgent;
}
