package tech.firas.framework.pojo;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@lombok.EqualsAndHashCode
@lombok.ToString
public final class WebClientInfo implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @Getter @Setter private String operatorIp;

    @Getter @Setter private String userAgent;

    @Getter @Setter private UUID operatorId;
}
