package tech.firas.framework.pojo;

public enum DataType {
    UNDEFINED, // 0
    INT32, // 1
    INT64, // 2
    VARCHAR, // 3
    DATE_TIME, // 4
    UUID, // 5
    CLOB, // 6
    BLOB, // 7
    DATE, // 8
    TIME, // 9
    DECIMAL // 10
}
