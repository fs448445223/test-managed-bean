package tech.firas.framework.pojo;

import javax.validation.constraints.Min;

import lombok.Getter;
import lombok.Setter;

@lombok.EqualsAndHashCode
public class MyPageRequest implements java.io.Serializable {

    @Getter @Setter
    @Min(0)
    private int page = 0;

    @Getter @Setter 
    @Min(1)
    private int size = 10;

    @Override public String toString() {
        return "{\"page\":" + this.page + ",\"size\":" + size + '}';
    }

    private static final long serialVersionUID = 1L;
}
