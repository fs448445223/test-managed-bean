package tech.firas.framework.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.domain.*;
import lombok.Getter;

@lombok.EqualsAndHashCode
public class MyPage<T> implements java.io.Serializable {

    public MyPage(Page<T> source) {
        this.totalElements = source.getTotalElements();
        this.totalPages = source.getTotalPages();
        this.number = source.getNumber();
        this.size = source.getSize();

        this.content = source.getContent();

        this.first = source.isFirst();
        this.last = source.isLast();
    }

    @JsonView(DefaultJsonView.class)
    @Getter private long totalElements;

    @JsonView(DefaultJsonView.class)
    @Getter private int totalPages;

    @JsonView(DefaultJsonView.class)
    @Getter private int number;

    @JsonView(DefaultJsonView.class)
    @Getter private int size;

    @JsonView(DefaultJsonView.class)
    @Getter private List<T> content;

    @JsonView(DefaultJsonView.class)
    @Getter private boolean first;

    @JsonView(DefaultJsonView.class)
    @Getter private boolean last;

    private static final long serialVersionUID = 1L;
}
