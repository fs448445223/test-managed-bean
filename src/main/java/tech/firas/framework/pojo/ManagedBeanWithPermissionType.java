package tech.firas.framework.pojo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

import tech.firas.framework.po.ManagedBean;
import tech.firas.framework.po.PermissionBase;

@lombok.Data
public class ManagedBeanWithPermissionType implements Serializable {

    @Getter @Setter
    private ManagedBean bean;

    @Getter @Setter
    private PermissionBase.PermissionType permissionType;

    public static ManagedBeanWithPermissionType of(final ManagedBean bean,
            final PermissionBase.PermissionType permissionType) {
        final ManagedBeanWithPermissionType result = new ManagedBeanWithPermissionType();
        result.bean = bean;
        result.permissionType = permissionType;
        return result;
    }
}
