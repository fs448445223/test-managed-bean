package tech.firas.framework.pojo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class MyAuthenticationToken implements Authentication {

    private final Collection<GrantedAuthority> authorities;
    private final String username;
    private boolean authenticated = true;

    public MyAuthenticationToken(final String username, final Collection<? extends GrantedAuthority> authorities) {
        this.username = username;
        this.authorities = Collections.unmodifiableList(new ArrayList<GrantedAuthority>(authorities));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return this.username;
    }

    @Override
    public Object getPrincipal() {
        return this.username;
    }

    @Override
    public boolean isAuthenticated() {
        return this.authenticated;
    }

    @Override
    public void setAuthenticated(final boolean isAuthenticated) throws IllegalArgumentException {
        this.authenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return this.username;
    }
}
