package tech.firas.framework.repository;

import java.util.List;
import java.util.UUID;

import tech.firas.framework.po.UserPermission;

public interface UserPermissionRepository extends
        org.springframework.data.jpa.repository.JpaRepository<UserPermission, java.util.UUID>,
        org.springframework.data.jpa.repository.JpaSpecificationExecutor<UserPermission> {

    UserPermission getFirstByUserIdAndAndBeanId(UUID userId, UUID beanId);

    List<UserPermission> findByUserId(UUID userId);

    List<UserPermission> findByBeanId(UUID beanId);

    int deleteByBeanId(UUID beanId);

    int deleteByUserId(UUID userId);
}
