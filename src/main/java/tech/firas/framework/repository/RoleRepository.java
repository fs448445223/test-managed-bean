package tech.firas.framework.repository;

import tech.firas.framework.po.Role;

public interface RoleRepository extends
        org.springframework.data.jpa.repository.JpaRepository<Role, java.util.UUID>,
        org.springframework.data.jpa.repository.JpaSpecificationExecutor<Role> {
}
