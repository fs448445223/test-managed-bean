package tech.firas.framework.repository;

import tech.firas.framework.po.ManagedBean;

public interface ManagedBeanRepository extends
        org.springframework.data.jpa.repository.JpaRepository<ManagedBean, java.util.UUID>,
        org.springframework.data.jpa.repository.JpaSpecificationExecutor<ManagedBean> {
}
