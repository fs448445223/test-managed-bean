package tech.firas.framework.repository;

import java.util.UUID;

import tech.firas.framework.po.User;

public interface UserRepository extends
        org.springframework.data.jpa.repository.JpaRepository<User, UUID>,
        org.springframework.data.jpa.repository.JpaSpecificationExecutor<User> {

    User getFirstByUsername(String username);
}
