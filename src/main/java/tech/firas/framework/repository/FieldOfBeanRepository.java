package tech.firas.framework.repository;

import java.util.Collection;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import tech.firas.framework.po.FieldOfBean;

public interface FieldOfBeanRepository extends
        org.springframework.data.jpa.repository.JpaRepository<FieldOfBean, UUID>,
        org.springframework.data.jpa.repository.JpaSpecificationExecutor<FieldOfBean> {

    @Modifying
    @Query("DELETE FROM FieldOfBean f WHERE f.id IN ?1")
    void deleteByIdCollection(Collection<UUID> idCollection);
}
