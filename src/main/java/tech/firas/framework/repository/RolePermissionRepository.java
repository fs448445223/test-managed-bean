package tech.firas.framework.repository;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import tech.firas.framework.po.RolePermission;

public interface RolePermissionRepository extends
        org.springframework.data.jpa.repository.JpaRepository<RolePermission, java.util.UUID>,
        org.springframework.data.jpa.repository.JpaSpecificationExecutor<RolePermission> {

    RolePermission getFirstByRoleIdAndBeanId(UUID roleId, UUID beanId);

    List<RolePermission> findByRoleId(UUID roleId);

    List<RolePermission> findByBeanId(UUID beanId);

    List<RolePermission> findByRoleIdInOrderByWeightAsc(Collection<UUID> roleIds);

    int deleteByBeanId(UUID beanId);

    int deleteByRoleId(UUID roleId);
}
