package tech.firas.framework.repository;

import java.util.Collection;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import tech.firas.framework.po.ValidationRule;

public interface ValidationRuleRepository extends
        org.springframework.data.jpa.repository.JpaRepository<ValidationRule, UUID>,
        org.springframework.data.jpa.repository.JpaSpecificationExecutor<ValidationRule> {

    @Modifying
    @Query("DELETE FROM ValidationRule f WHERE f.id IN ?1")
    void deleteByIdCollection(Collection<UUID> idCollection);
}
