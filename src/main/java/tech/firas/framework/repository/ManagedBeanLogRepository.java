package tech.firas.framework.repository;

import java.util.UUID;

import tech.firas.framework.po.ManagedBeanLog;

public interface ManagedBeanLogRepository extends
        org.springframework.data.jpa.repository.JpaRepository<ManagedBeanLog, UUID>,
        org.springframework.data.jpa.repository.JpaSpecificationExecutor<ManagedBeanLog> {
}
