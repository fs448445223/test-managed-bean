package tech.firas.framework.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import lombok.Getter;
import lombok.Setter;

@lombok.extern.slf4j.Slf4j
public class ByFieldNameBeanConverter<S, D> implements java.util.function.Function<S, D> {

    private final Constructor<D> constructor;
    private final Map<FieldGetter<S>, FieldSetter<D>> map;
    // private final boolean tryBestConvert;

    public ByFieldNameBeanConverter(final Class<S> srcClass, final Class<D> destClass,
            final Configuration configuration) throws NoSuchMethodException {
        final Configuration conf = configuration == null ? new Configuration() : configuration;
        try {
            this.constructor = destClass.getConstructor();
            this.constructor.newInstance();
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            throw new NoSuchMethodException("There is no accessible default constructor (no parameter) for " +
                    destClass.getName());
        }

        // this.tryBestConvert = conf.tryBestConvert;

        this.map = new HashMap<>();
        for (Class<?> clazz = srcClass; !Object.class.equals(clazz) && clazz != null; clazz = clazz.getSuperclass()) {
            for (final Field field : clazz.getDeclaredFields()) {
                if (Modifier.isStatic(field.getModifiers()) ||
                        ( conf.isAllowGetTransient() && Modifier.isTransient(field.getModifiers()) )) {
                    continue;
                }
                try {
                    final FieldGetter<S> getter = new FieldGetter<>(srcClass, field, conf);
                    final FieldSetter<D> setter = getCorrespondingSetter(field.getName(),
                            getter.getReturnType(), destClass, conf);
                    if (null != setter) {
                        map.put(getter, setter);
                        if (log.isDebugEnabled()) {
                            log.debug("from: " + srcClass.getName() + ", to: " + destClass.getName() +
                                    ", field: " + field.getName() + ", fromDeclaringClass: " + clazz.getName());
                        }
                    }
                } catch (NoSuchMethodException ex) {
                    log.debug(ex.getMessage());
                }
            }
        }
    }

    @Override
    public D apply(final S src) {
        if (null == src) {
            return null;
        }
        try {
            final D dest = this.constructor.newInstance();
            for (final Map.Entry<FieldGetter<S>, FieldSetter<D>> entry : map.entrySet()) {
                final Object value = entry.getKey().get(src);
                entry.getValue().set(dest, value);
            }
            return dest;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            throw new RuntimeException("Fail to convert an instance of \"" + src.getClass().getName() +
                    "\" to an instance of \"" + this.constructor.getDeclaringClass().getName() + '\"', ex);
        }
    }

    private static <D> FieldSetter<D> getCorrespondingSetter(final String fieldName,
            final Class<?> srcFieldType, final Class<D> destClass,
            final Configuration configuration) throws NoSuchMethodException {
        for (Class<?> clazz = destClass; !Object.class.equals(clazz) && clazz != null; clazz = clazz.getSuperclass()) {
            for (final Field field : clazz.getDeclaredFields()) {
                if (!Modifier.isStatic(field.getModifiers()) &&
                        (configuration.isAllowSetTransient() || !Modifier.isTransient(field.getModifiers())) &&
                        field.getName().equals(fieldName) &&
                        field.getType().isAssignableFrom(srcFieldType)) {
                    final FieldSetter<D> setter = new FieldSetter<>(destClass, field, configuration);
                    if (setter.getParameterType().isAssignableFrom(srcFieldType)) {
                        return setter;
                    }
                }
            }
        }
        return null;
    }

    public static final class Configuration {
        /**
         * whether allow using getXXX method to get a boolean field
         */
        @Getter @Setter private boolean allowGetBoolean;

        /**
         * whether try to access the field directly when there is no getter
         */
        @Getter @Setter private boolean allowDirectlyGetField;

        /**
         * whether try to access the field directly when there is no setter
         */
        @Getter @Setter private boolean allowDirectlySetField;

        @Getter @Setter private boolean allowGetTransient;

        @Getter @Setter private boolean allowSetTransient;

        /**
         * try to convert among Number, Date, Temporal and String
         */
        // @Getter @Setter private boolean tryBestConvert;
    }

    private static final class FieldGetter<E> {

        private final Field field;
        private final Method getMethod;

        private FieldGetter(final Class<? super E> srcClass, final Field field, final Configuration configuration)
                throws NoSuchMethodException {
            this.field = field;
            final Class<?> fieldType = field.getType();
            final Class<?> declaringClass = field.getDeclaringClass();

            final Pattern getterPattern = Pattern.compile(
                    (fieldType.equals(boolean.class) ?
                            (configuration.isAllowGetBoolean() ? "(get|is)" : "is") : "get") +
                    field.getName().substring(0, 1).toUpperCase(Locale.US) + field.getName().substring(1));
            for (Class<?> clazz = srcClass; declaringClass.isAssignableFrom(clazz); clazz = clazz.getSuperclass()) {
                for (final Method method : clazz.getMethods()) {
                    if (getterPattern.matcher(method.getName()).matches() &&
                            method.getReturnType().isAssignableFrom(fieldType) &&
                            method.getParameterCount() == 0) {
                        this.getMethod = method;
                        return;
                    }
                }
            }
            if (!configuration.allowDirectlyGetField) {
                throw new NoSuchMethodException("Found no getter for \"" + field.getName() + "\" in \"" +
                        srcClass.getName() + '\"');
            }
            this.getMethod = null;
            field.setAccessible(true);
        }

        private Class<?> getReturnType() {
            if (this.getMethod == null) {
                return this.field.getType();
            }
            return this.getMethod.getReturnType();
        }

        private Object get(final E obj) throws IllegalAccessException, InvocationTargetException {
            if (this.getMethod == null) {
                return field.get(obj);
            }
            return this.getMethod.invoke(obj);
        }

        @Override
        public boolean equals(Object other) {
            if (!(other instanceof FieldGetter)) {
                return false;
            }
            return this.field.getName().equals( ((FieldGetter)other).field.getName() );
        }

        @Override
        public int hashCode() {
            return this.field.getName().hashCode();
        }
    }

    private static final class FieldSetter<E> {

        private final Field field;
        private final Method setMethod;

        private FieldSetter(final Class<? super E> destClass, final Field field, final Configuration configuration)
                throws NoSuchMethodException {
            this.field = field;
            final Class<?> fieldType = field.getType();
            final Class<?> declaringClass = field.getDeclaringClass();

            final String setterName = "set" + field.getName().substring(0, 1).toUpperCase(Locale.US) +
                    field.getName().substring(1);
            for (Class<?> clazz = destClass; declaringClass.isAssignableFrom(clazz); clazz = clazz.getSuperclass()) {
                for (final Method method : clazz.getMethods()) {
                    if (setterName.equals(method.getName()) &&
                            method.getParameterCount() == 1 &&
                            fieldType.isAssignableFrom(method.getParameterTypes()[0])) {
                        this.setMethod = method;
                        return;
                    }
                }
            }
            if (!configuration.allowDirectlyGetField) {
                throw new NoSuchMethodException("Found no setter for \"" + field.getName() + "\" in \"" +
                        destClass.getName() + '\"');
            }
            this.setMethod = null;
            field.setAccessible(true);
        }

        private Class<?> getParameterType() {
            if (this.setMethod == null) {
                return this.field.getType();
            }
            return this.setMethod.getParameterTypes()[0];
        }

        private void set(E obj, Object value) throws InvocationTargetException, IllegalAccessException {
            if (this.setMethod == null) {
                this.field.set(obj, value);
            } else {
                this.setMethod.invoke(obj, value);
            }
        }

        @Override
        public boolean equals(Object other) {
            if (!(other instanceof FieldGetter)) {
                return false;
            }
            return this.field.getName().equals( ((FieldGetter)other).field.getName() );
        }

        @Override
        public int hashCode() {
            return this.field.getName().hashCode();
        }
    }
}
