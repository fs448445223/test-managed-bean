package tech.firas.framework.event;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import org.springframework.util.Assert;

public class EventManager<T> {

    private Map<String, List<Consumer<T>>> listenerMap = new ConcurrentHashMap<>();

    public void addEventListener(final String eventName, final Consumer<T> listener) {
        Assert.notNull(eventName, "eventName should not be null");
        Assert.notNull(listener, "listener should not be null");
        listenerMap.computeIfAbsent(eventName, k -> new Vector<>()).add(listener);
    }

    public void removeEventListener(final String eventName, final Consumer<T> listener) {
        Assert.notNull(eventName, "eventName should not be null");
        Assert.notNull(listener, "listener should not be null");
        listenerMap.computeIfAbsent(eventName, k -> new Vector<>()).remove(listener);
    }

    public void triggerEvent(final String eventName, final T param) {
        Assert.notNull(eventName, "eventName should not be null");
        listenerMap.getOrDefault(eventName, Collections.emptyList()).forEach(listener -> listener.accept(param));
    }
}
