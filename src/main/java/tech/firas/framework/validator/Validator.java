package tech.firas.framework.validator;

import java.math.BigDecimal;
import java.util.regex.Pattern;
import javax.validation.ValidationException;

import tech.firas.framework.po.FieldOfBean;
import tech.firas.framework.po.ValidationRule;
import tech.firas.framework.pojo.DataType;

public class Validator {

    private Validator() {}

    public static void validate(ValidationRule validationRule, String fieldValue) {
        final DataType fieldDataType = validationRule.getField().getDataType();
        Pattern pattern;
        switch (validationRule.getValidationType()) {
            case ValidationRule.TYPE_NULL:
                if (null != fieldValue) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            case ValidationRule.TYPE_NOT_NULL:
                if (null == fieldValue) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            case ValidationRule.TYPE_MIN:
                if (null == fieldValue) {
                    break; // allow null
                }
                validateMin(validationRule, fieldDataType, fieldValue);
                break;
            case ValidationRule.TYPE_MAX:
                if (null == fieldValue) {
                    break; // allow null
                }
                validateMax(validationRule, fieldDataType, fieldValue);
                break;
            case ValidationRule.TYPE_MATCH:
                if (null == fieldValue) {
                    break; // allow null
                }
                pattern = Pattern.compile(validationRule.getValidationValue());
                if ( ! pattern.matcher(fieldValue).find() ) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            case ValidationRule.TYPE_NOT_MATCH:
                if (null == fieldValue) {
                    break; // allow null
                }
                pattern = Pattern.compile(validationRule.getValidationValue());
                if ( pattern.matcher(fieldValue).find() ) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
        }
    }

    private static void validateMin(final ValidationRule validationRule,
            final DataType fieldDataType, final String fieldValue) {
        switch (fieldDataType) {
            case INT32: {
                final int actualValue;
                try {
                    actualValue = Integer.parseInt(fieldValue);
                } catch (NumberFormatException ex) {
                    throw new ValidationException(validationRule.getErrorMessage(), ex);
                }
                int minValue = Integer.parseInt(validationRule.getValidationValue());
                if (actualValue < minValue) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            }
            case INT64: {
                final long actualValue;
                try {
                    actualValue = Long.parseLong(fieldValue);
                } catch (NumberFormatException ex) {
                    throw new ValidationException(validationRule.getErrorMessage(), ex);
                }
                long minValue = Long.parseLong(validationRule.getValidationValue());
                if (actualValue < minValue) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            }
            case VARCHAR: {
                final int minValue = Integer.parseInt(validationRule.getValidationValue());
                if (fieldValue.length() < minValue) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            }
            case DECIMAL: {
                final BigDecimal actualValue;
                try {
                    actualValue = new BigDecimal(fieldValue);
                } catch (NumberFormatException ex) {
                    throw new ValidationException(validationRule.getErrorMessage(), ex);
                }
                final BigDecimal minValue = new BigDecimal(validationRule.getValidationValue());
                if (actualValue.compareTo(minValue) < 0) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            }
        }
    }

    private static void validateMax(final ValidationRule validationRule,
            final DataType fieldDataType, final String fieldValue) {
        switch (fieldDataType) {
            case INT32: {
                final int actualValue;
                try {
                    actualValue = Integer.parseInt(fieldValue);
                } catch (NumberFormatException ex) {
                    throw new ValidationException(validationRule.getErrorMessage(), ex);
                }
                int maxValue = Integer.parseInt(validationRule.getValidationValue());
                if (actualValue > maxValue) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            }
            case INT64: {
                final long actualValue;
                try {
                    actualValue = Long.parseLong(fieldValue);
                } catch (NumberFormatException ex) {
                    throw new ValidationException(validationRule.getErrorMessage(), ex);
                }
                long maxValue = Long.parseLong(validationRule.getValidationValue());
                if (actualValue > maxValue) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
            }
            case VARCHAR: {
                final int maxValue = Integer.parseInt(validationRule.getValidationValue());
                if (fieldValue.length() > maxValue) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            }
            case DECIMAL: {
                final BigDecimal actualValue;
                try {
                    actualValue = new BigDecimal(fieldValue);
                } catch (NumberFormatException ex) {
                    throw new ValidationException(validationRule.getErrorMessage(), ex);
                }
                final BigDecimal maxValue = new BigDecimal(validationRule.getValidationValue());
                if (actualValue.compareTo(maxValue) > 0) {
                    throw new ValidationException(validationRule.getErrorMessage());
                }
                break;
            }
        }
    }
}
