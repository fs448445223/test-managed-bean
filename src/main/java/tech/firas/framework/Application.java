package tech.firas.framework;

import javax.validation.ValidationException;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import tech.firas.framework.event.EventManager;
import tech.firas.framework.pojo.MyResponse;
import tech.firas.framework.service.ManagedBeanService;
import tech.firas.framework.service.ManagedBeanServiceImpl;
import tech.firas.framework.service.UsernamePasswordAuthenticationProvider;

@Configuration
@org.springframework.boot.autoconfigure.EnableAutoConfiguration
@org.springframework.context.annotation.ComponentScan({
        "tech.firas.framework.controller", "tech.firas.framework.service"
})
@org.springframework.boot.autoconfigure.domain.EntityScan({
        "tech.firas.framework.po"
})
@org.springframework.data.jpa.repository.config.EnableJpaRepositories({
        "tech.firas.framework.repository"
})
@lombok.extern.slf4j.Slf4j
public class Application {

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }

    @Bean
    public ObjectMapper jsonConverter() {
        return new ObjectMapper();
    }

    @Bean
    public EventManager<Object> eventManager() {
        return new EventManager<>();
    }

    @Bean
    public ManagedBeanService managedBeanService() {
        final ManagedBeanServiceImpl bean = new ManagedBeanServiceImpl();
        bean.setSqlDialect(tech.firas.framework.dialect.PostgreDialect.getInstance());
        return bean;
    }

    /*
    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        return builder.setType(EmbeddedDatabaseType.HSQL).build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("tech.firas.framework.po");
        factory.setDataSource(dataSource);
        return factory;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory);
        return txManager;
    }
    */

    @org.springframework.web.bind.annotation.ControllerAdvice
    public class GlobalExceptionHandler {

        @ExceptionHandler(ValidationException.class)
        @ResponseBody
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        public MyResponse<String> handle(final ValidationException ex) {
            return new MyResponse<>("invalidRequest", "invalidRequest", ex.getMessage());
        }

        @ExceptionHandler(AccessDeniedException.class)
        @ResponseBody
        @ResponseStatus(HttpStatus.NOT_FOUND)
        public String handle(final AccessDeniedException ex) {
            log.info("Access Denied", ex);
            return "Not Found";
        }
    }

    @Configuration
    public static class WebMvcConfig extends org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport {

        @Override
        protected void addCorsMappings(final org.springframework.web.servlet.config.annotation.CorsRegistry registry) {
            final org.springframework.context.ApplicationContext applicationContext = this.getApplicationContext();
            assert applicationContext != null;
            final org.springframework.core.env.Environment environment = applicationContext.getEnvironment();
            final String origin = environment.getProperty("app.protocol", "https") + "://" +
                    environment.getProperty("app.host") + ':' + environment.getProperty("app.port", "8080");
            if (origin.matches("http://.*:80") || origin.matches("https://.*:443")) {
                log.debug("default port");
                origin = origin.replaceFirst(":(80|443)$", "");
            }
            log.debug("addCorsMappings: " + origin);
            registry.addMapping("/**").allowedOrigins(origin)
                    .allowedMethods("GET", "HEAD", "POST", "PATCH", "PUT", "DELETE", "OPTIONS")
                    //.allowedHeaders("Content-type", "Accept", "Origin")
                    .allowCredentials(false).maxAge(3600);
        }
    }

    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {}

    @EnableWebSecurity
    public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

        private UsernamePasswordAuthenticationProvider usernamePasswordAuthenticationProvider;
        private UserDetailsService userDetailsServiceImpl;

        @Autowired
        public void setUsernamePasswordAuthenticationProvider(
                final UsernamePasswordAuthenticationProvider usernamePasswordAuthenticationProvider) {
            this.usernamePasswordAuthenticationProvider = usernamePasswordAuthenticationProvider;
        }

        @Autowired
        public void setUserDetailsServiceImpl(
                final UserDetailsService userDetailsServiceImpl) {
            this.userDetailsServiceImpl = userDetailsServiceImpl;
        }

        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/error").permitAll()
                    .antMatchers(HttpMethod.POST, "/login").permitAll()
                    .antMatchers(HttpMethod.POST, "/logout").authenticated()
                    .antMatchers(HttpMethod.GET, "/permission").authenticated()
                    .antMatchers(HttpMethod.GET, "/managedBean/**").authenticated()
                    .antMatchers(HttpMethod.POST, "/managedBean/**").authenticated()
                    .antMatchers(HttpMethod.DELETE, "/managedBean/**").authenticated()
                    .and()
                    .csrf().disable()
                    .formLogin()
                            .successHandler(this.usernamePasswordAuthenticationProvider)
                            .failureHandler(this.usernamePasswordAuthenticationProvider)
                            .authenticationDetailsSource(this.usernamePasswordAuthenticationProvider)
                    .and()
                    .exceptionHandling()
                            .authenticationEntryPoint(this.usernamePasswordAuthenticationProvider)
                    .and()
                    .logout()
                            .logoutSuccessHandler(this.usernamePasswordAuthenticationProvider)
                            .permitAll();
        }
    }
}
