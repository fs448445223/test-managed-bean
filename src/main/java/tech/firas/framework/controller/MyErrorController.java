package tech.firas.framework.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

@lombok.extern.slf4j.Slf4j
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/error")
public class MyErrorController implements org.springframework.boot.web.servlet.error.ErrorController {

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> handleJson(final HttpServletRequest request) {
        final HttpStatus status = this.getStatus(request);
        if (status == HttpStatus.NO_CONTENT) {
            return new ResponseEntity<>(status);
        } else {
            final String body = "{\"statusCode\":" + status.value() + ",\"status\":\"" +
                    status.getReasonPhrase() + "\",\"timestamp\":" + System.currentTimeMillis() + '}';
            return new ResponseEntity<>(body, status);
        }
    }

    @RequestMapping
    public ResponseEntity<String> handleText(final HttpServletRequest request) {
        final HttpStatus status = this.getStatus(request);
        if (status == HttpStatus.NO_CONTENT) {
            return new ResponseEntity<>(status);
        } else {
            return new ResponseEntity<>(status.getReasonPhrase(), status);
        }
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
    
    private HttpStatus getStatus(final HttpServletRequest request) {
        Integer statusCode = (Integer)request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } else {
            try {
                return HttpStatus.valueOf(statusCode);
            } catch (Exception var4) {
                return HttpStatus.INTERNAL_SERVER_ERROR;
            }
        }
    }
}
