package tech.firas.framework.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.firas.framework.po.ManagedBean;
import tech.firas.framework.pojo.DefaultJsonView;
import tech.firas.framework.pojo.MyPage;
import tech.firas.framework.pojo.MyPageRequest;
import tech.firas.framework.pojo.MyResponse;
import tech.firas.framework.service.ManagedBeanService;

@lombok.extern.slf4j.Slf4j
@Validated
@RestController
@RequestMapping("/managed-bean")
public class ManagedBeanController {

    // ----==== 0 means ManagedBean itself ====----
    @GetMapping("/0")
    @JsonView(DefaultJsonView.class)
    public MyResponse<MyPage<ManagedBean>> listByPage(
        @Valid MyPageRequest myPageRequest
    ) {
        return new MyResponse<>(this.managedBeanService.listByPage(myPageRequest));
    }

    @GetMapping("/0/{id}")
    @JsonView(ManagedBean.ViewWithFields.class)
    public MyResponse<?> getById(@NotNull @PathVariable UUID id) {
        if (log.isDebugEnabled()) log.debug("id: " + id);
        try {
            Optional<ManagedBean> managedBean = this.managedBeanService.getById(id);
            return managedBean.isPresent() ? new MyResponse<>(managedBean.get()) :
                    new MyResponse<>("id.notFound.getById.managedBean",
                            "The ManagedBean with the specified ID does not exist", id);
        } catch (final AccessDeniedException ex) {
            throw ex;
        } catch (final Exception ex) {
            log.error("Fail to get ManagedBean by ID: " + id, ex);
            return new MyResponse<>("fail.getById.ManagedBean", "Fail to get ManagedBean by ID: " + id, ex.getMessage());
        }
    }

    @PostMapping(path = "/0", consumes = MediaType.APPLICATION_JSON_VALUE)
    @JsonView(ManagedBean.ViewWithFields.class)
    public MyResponse<?> insertOrUpdateJson(@Valid @NotNull @RequestBody ManagedBean managedBean) {
        if (log.isDebugEnabled()) {
            log.debug("name: " + managedBean.getName());
            log.debug( "size of fields: " + (managedBean.getFields() == null ? "null" : managedBean.getFields().size()) );
        }

        try {
            return new MyResponse<>( this.managedBeanService.insertOrUpdate(managedBean) );

        } catch (final AccessDeniedException ex) {
            throw ex;
        } catch (final Exception ex) {
            if ( ManagedBeanService.ErrorCode.BEAN_NAME_CONFLICT.equalsCode(ex.getMessage()) ) {
                return new MyResponse<>(ManagedBeanService.ErrorCode.BEAN_NAME_CONFLICT,
                        "The name has been occupied by another ManagedBean", managedBean.getName());

            } else if ( ManagedBeanService.ErrorCode.FIELD_NAME_BLANK.equalsCode(ex.getMessage()) ) {
                return new MyResponse<>(ManagedBeanService.ErrorCode.FIELD_NAME_BLANK,
                        "The name of a FieldOfBean must not be blank", managedBean.getName());

            } else if ( ManagedBeanService.ErrorCode.FIELD_NAME_CONFLICT.equalsCode(ex.getMessage()) ) {
                return new MyResponse<>(ManagedBeanService.ErrorCode.BEAN_NAME_CONFLICT,
                        "The name of the fields of a ManagedBean must be unique", managedBean.getName());

            } else {
                String message = "Fail to insert or update the ManagedBean";
                log.error(message, ex);
                return new MyResponse<String>(ManagedBeanService.ErrorCode.UNKNOWN,
                        message, null);
            }
        }
    }

    // ----==== other beans ====----
    @GetMapping("/{name}")
    public MyResponse<?> listBeanByPage(
        @NotNull @Size(min = 1, max = 40) @PathVariable String name,
        @Valid MyPageRequest myPageRequest
    ) {
        if (log.isDebugEnabled()) log.debug("name: " + name);
        try {
            Optional<ManagedBean> beanOptional = this.managedBeanService.getByName(name);
            if ( ! beanOptional.isPresent() ) {
                return new MyResponse<>(ManagedBeanService.ErrorCode.BEAN_NAME_NOT_EXIST,
                        "The ManagedBean with the specified name does not exists", name);
            }

            PageRequest pageRequest = PageRequest.of(myPageRequest.getPage(), myPageRequest.getSize());
            Page<Map<String, Object>> page = this.managedBeanService.listBeanByPage(beanOptional.get(), pageRequest);
            return new MyResponse<>(new MyPage<>(page));
        } catch (final AccessDeniedException ex) {
            throw ex;
        } catch (final Exception ex) {
            String message = "Fail to list the bean " + name + " by page. " + myPageRequest;
            log.error(message, ex);
            return new MyResponse<>(ManagedBeanService.ErrorCode.UNKNOWN, message, ex.getMessage());
        }
    }

    @GetMapping("/{name}/{id}")
    public MyResponse<?> getBeanById(
        @NotNull @Size(min = 1, max = 40) @PathVariable String name,
        @NotNull @PathVariable UUID id
    ) {
        if (log.isDebugEnabled()) log.debug("name: " + name);
        try {
            Optional<ManagedBean> beanOptional = this.managedBeanService.getByName(name);
            if ( ! beanOptional.isPresent() ) {
                return new MyResponse<>(ManagedBeanService.ErrorCode.BEAN_NAME_NOT_EXIST,
                        "The ManagedBean with the specified name does not exists", name);
            }

            @SuppressWarnings("unchecked")
            Serializable bean = (Serializable) this.managedBeanService.getBeanById( beanOptional.get(), id );
            return new MyResponse<>(bean);
        } catch (final AccessDeniedException ex) {
            throw ex;
        } catch (final Exception ex) {
            String message = "Fail to get the bean " + name + " by id " + id;
            log.error(message, ex);
            return new MyResponse<>(ManagedBeanService.ErrorCode.UNKNOWN, message, ex.getMessage());
        }
    }

    @PostMapping(path = "/{name}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public MyResponse<?> insertOrUpdateBean(
        @NotNull @Size(min = 1, max = 40) @PathVariable String name,
        @NotNull @RequestBody Map<String, Object> bean
    ) {
        if (log.isDebugEnabled()) log.debug("name: " + name);
        try {
            Optional<ManagedBean> beanOptional = this.managedBeanService.getByName(name);
            if ( ! beanOptional.isPresent() ) {
                return new MyResponse<>(ManagedBeanService.ErrorCode.BEAN_NAME_NOT_EXIST,
                        "The ManagedBean with the specified name does not exists", name);
            }

            return new MyResponse<>((Serializable) this.managedBeanService.insertOrUpdateBean(
                    beanOptional.get(), bean));
        } catch (final AccessDeniedException ex) {
            throw ex;
        } catch (final Exception ex) {
            String message = "Fail to insert / update the bean " + name;
            log.error(message, ex);
            return new MyResponse<>(ManagedBeanService.ErrorCode.UNKNOWN, message, ex.getMessage());
        }
    }
    /*
    public MyResponse<?> insertOrUpdateBeans(@NotNull @RequestBody Map< String, List<Map<String, Object>> > beans) {
        return null;
    }
    */

    private ManagedBeanService managedBeanService;
    @Inject
    public void setManagedBeanService(ManagedBeanService managedBeanService) {
        this.managedBeanService = managedBeanService;
    }
}
