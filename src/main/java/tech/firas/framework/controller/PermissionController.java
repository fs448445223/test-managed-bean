package tech.firas.framework.controller;

import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.firas.framework.pojo.ManagedBeanWithPermissionType;
import tech.firas.framework.pojo.MyResponse;
import tech.firas.framework.service.ManagedBeanService;

@lombok.extern.slf4j.Slf4j
@RestController
@RequestMapping("/permission")
public class PermissionController {

    @GetMapping
    public <T extends Serializable & List<ManagedBeanWithPermissionType>> MyResponse<T> listPermittedBeans() {
        final T result = this.managedBeanService.listPermittedBeans();
        log.debug("result.size: " + (result == null ? "null" : result.size()));
        return new MyResponse<>(result);
    }

    private ManagedBeanService managedBeanService;
    @Inject
    public void setManagedBeanService(ManagedBeanService managedBeanService) {
        this.managedBeanService = managedBeanService;
    }
}
