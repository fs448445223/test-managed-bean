package tech.firas.framework.dialect;

import tech.firas.framework.dialect.resultset.ResultSetRetriever;
import tech.firas.framework.pojo.DataType;

public class MySQLDialect extends SqlDialect {

    private MySQLDialect() {}
    private static final MySQLDialect singleton = new MySQLDialect();
    public static MySQLDialect getInstance() {
        return singleton;
    }

    @Override
    public String quote(String identifier) {
        return '`' + identifier + '`';
    }

    @Override
    public String getDataTypeName(final DataType dataType) {
        switch (dataType) {
            case CLOB:
                return "TEXT";
            case BLOB:
                return "BLOB";
            case DATE_TIME:
                return "DATETIME";
            case DATE:
                return "DATE";
            case TIME:
                return "TIME";
        }
        return super.getDataTypeName(dataType);
    }

    @Override
    public ResultSetRetriever getResultSetRetriever() {
        throw new UnsupportedOperationException();
    }
}
