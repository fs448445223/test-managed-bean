package tech.firas.framework.dialect.resultset;

public class H2ResultSetRetriever extends ResultSetRetriever {

    private static final H2ResultSetRetriever instance = new H2ResultSetRetriever();
    public static H2ResultSetRetriever getInstance() {
        return instance;
    }
    private H2ResultSetRetriever() {}
}
