package tech.firas.framework.dialect.resultset;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.UUID;

public abstract class ResultSetRetriever {

    public Integer getInteger(final ResultSet resultSet, final String columnName) throws SQLException {
        final int value = resultSet.getInt(columnName);
        return resultSet.wasNull() ? null : value;
    }

    public Long getLong(final ResultSet resultSet, final String columnName) throws SQLException {
        final long value = resultSet.getLong(columnName);
        return resultSet.wasNull() ? null : value;
    }

    public String getString(final ResultSet resultSet, final String columnName) throws SQLException {
        return resultSet.getString(columnName);
    }

    public Timestamp getDateTime(final ResultSet resultSet, final String columnName) throws SQLException {
        return resultSet.getTimestamp(columnName);
    }

    public java.sql.Date getDate(final ResultSet resultSet, final String columnName) throws SQLException {
        return resultSet.getDate(columnName);
    }

    public Time getTime(final ResultSet resultSet, final String columnName) throws SQLException {
        return resultSet.getTime(columnName);
    }

    public Clob getClob(final ResultSet resultSet, final String columnName) throws SQLException {
        return resultSet.getClob(columnName);
    }

    public Blob getBlob(final ResultSet resultSet, final String columnName) throws SQLException {
        return resultSet.getBlob(columnName);
    }

    public UUID getUuid(final ResultSet resultSet, final String columnName) throws SQLException {
        return resultSet.getObject(columnName, UUID.class);
    }
}
