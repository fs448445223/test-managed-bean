package tech.firas.framework.dialect.resultset;

public class PostgreResultSetRetriever extends ResultSetRetriever {

    private static final PostgreResultSetRetriever instance = new PostgreResultSetRetriever();
    public static PostgreResultSetRetriever getInstance() {
        return instance;
    }
    private PostgreResultSetRetriever() {}
}
