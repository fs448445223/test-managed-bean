package tech.firas.framework.dialect;

import tech.firas.framework.dialect.resultset.H2ResultSetRetriever;
import tech.firas.framework.dialect.resultset.ResultSetRetriever;

public class H2Dialect extends SqlDialect {

    private H2Dialect() {}
    private static final H2Dialect singleton = new H2Dialect();
    public static H2Dialect getInstance() {
        return singleton;
    }

    @Override
    public String quote(String identifier) {
        return '"' + identifier + '"';
    }

    @Override
    public ResultSetRetriever getResultSetRetriever() {
        return H2ResultSetRetriever.getInstance();
    }
}
