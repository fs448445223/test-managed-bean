package tech.firas.framework.dialect.feature;

public class MySQLFeature extends SqlFeature {

    private static final MySQLFeature instance = new MySQLFeature();
    public static MySQLFeature getInstance() {
        return instance;
    }

    @Override
    public boolean supportJsonType() {
        return true;
    }

    private MySQLFeature() {}
}
