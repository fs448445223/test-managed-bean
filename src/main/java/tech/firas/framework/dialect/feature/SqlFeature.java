package tech.firas.framework.dialect.feature;

public abstract class SqlFeature {

    public boolean supportBooleanType() {
        return false;
    }

    public boolean supportUuidType() {
        return false;
    }

    public boolean supportJsonType() {
        return false;
    }


    public boolean supportRowLock() {
        return false;
    }

    public boolean supportTransactionalDDL() {
        return false;
    }
}
