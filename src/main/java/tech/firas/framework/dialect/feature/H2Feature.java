package tech.firas.framework.dialect.feature;

public class H2Feature extends SqlFeature {

    private static final H2Feature instance = new H2Feature();
    public static H2Feature getInstance() {
        return instance;
    }
    private  H2Feature() {}
}
