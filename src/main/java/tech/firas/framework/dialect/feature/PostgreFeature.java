package tech.firas.framework.dialect.feature;

public class PostgreFeature extends SqlFeature {

    private static final PostgreFeature instance = new PostgreFeature();
    public static PostgreFeature getInstance() {
        return instance;
    }

    @Override
    public boolean supportBooleanType() {
        return true;
    }

    @Override
    public boolean supportUuidType() {
        return true;
    }

    private PostgreFeature() {}
}
