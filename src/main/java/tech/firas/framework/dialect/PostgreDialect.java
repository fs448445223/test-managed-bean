package tech.firas.framework.dialect;

import tech.firas.framework.dialect.resultset.PostgreResultSetRetriever;
import tech.firas.framework.dialect.resultset.ResultSetRetriever;
import tech.firas.framework.po.ManagedBean;
import tech.firas.framework.pojo.DataType;

public class PostgreDialect extends SqlDialect {

    private PostgreDialect() {}
    private static final PostgreDialect singleton = new PostgreDialect();
    public static PostgreDialect getInstance() {
        return singleton;
    }
    
    @Override
    public String quote(String identifier) {
        return '"' + identifier + '"';
    }

    @Override
    public String getDataTypeName(final DataType dataType) {
        switch (dataType) {
            case UUID:
                return "UUID";
            case CLOB:
                return "TEXT";
            case BLOB:
                return "BYTEA";
            case DATE_TIME:
                return "TIMESTAMP";
        }
        return super.getDataTypeName(dataType);
    }

    @Override
    public ResultSetRetriever getResultSetRetriever() {
        return PostgreResultSetRetriever.getInstance();
    }

    @Override
    public String getSelectByIdSql(final ManagedBean managedBean, final String tableAlias) {
        return this.getSelectAllSql(managedBean, tableAlias) +
                " WHERE " + this.quoteColumn("id", tableAlias) + " = Cast(? as UUID)";
    }

    @Override
    public String getSelectIdByIdInSql(
            final ManagedBean managedBean, final String idAlias, final String tableAlias, final int idCount
    ) {
        if (idCount <= 0) {
            throw new IllegalArgumentException("idCount <= 0: " + idCount);
        }

        final StringBuilder buffer = new StringBuilder("SELECT ");
        buffer.append(quote("id"));
        if ( null != idAlias && ! idAlias.isEmpty() ) {
            buffer.append(" AS ").append(quote(idAlias));
        }
        buffer.append(" FROM ").append(this.getTableNameWithAlias(managedBean, tableAlias));
        buffer.append(" WHERE ");
        buffer.append(this.quoteColumn("id", tableAlias)).append(" IN (");

        final StringBuilder temp = new StringBuilder();
        for (int i = 0; i < idCount; i += 1) {
            temp.append(",Cast(? as UUID)");
        }
        buffer.append(temp.substring(1));

        buffer.append(')');
        return buffer.toString();
    }
}
