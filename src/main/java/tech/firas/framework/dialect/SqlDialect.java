package tech.firas.framework.dialect;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.UUID;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;

import tech.firas.framework.dialect.resultset.ResultSetRetriever;
import tech.firas.framework.po.FieldOfBean;
import tech.firas.framework.po.ManagedBean;
import tech.firas.framework.pojo.DataType;

public abstract class SqlDialect {

    public String quote(final String identifier) {
        return identifier;
    }

    public String quoteColumn(final String columnName, final String tableAlias) {
        final String c = this.quote(columnName);
        return (null == tableAlias || tableAlias.isEmpty()) ? c :
                ( this.quote(tableAlias) + '.' + c );
    }

    public String getTableName(final ManagedBean managedBean) {
        final String schema = managedBean.getDbSchema();
        return (null == schema || schema.isEmpty()) ? this.quote(managedBean.getDbTable()) :
                this.quote(schema) + '.' + this.quote(managedBean.getDbTable());
    }

    public String getTableNameWithAlias(final ManagedBean managedBean, final String tableAlias) {
        final String t = this.getTableName(managedBean);
        return (null == tableAlias || tableAlias.isEmpty()) ? t :
                ( t + " AS " + this.quote(tableAlias) );
    }

    public String getSelectAllSql(final ManagedBean managedBean, final String tableAlias) {
        final List<FieldOfBean> fields = managedBean.getFields();
        if (null == fields || fields.isEmpty()) {
            throw new IllegalArgumentException("managedBean.getFields() is null or empty");
        }

        final StringBuilder buffer = new StringBuilder("SELECT ");
        if (null == tableAlias || tableAlias.isEmpty()) {
            final StringBuilder temp = new StringBuilder();
            for (final FieldOfBean field : fields) {
                temp.append(',').append( quote(field.getDbColumn()) );
            }
            buffer.append(temp.substring(1));
            buffer.append(" FROM ");
            buffer.append(this.getTableName(managedBean));

        } else {
            final String quotedTableAlias = this.quote(tableAlias);
            final StringBuilder temp = new StringBuilder();
            for (final FieldOfBean field : fields) {
                temp.append(',').append(quotedTableAlias).append('.').append( quote(field.getDbColumn()) );
            }
            buffer.append(temp.substring(1));
            buffer.append(" FROM ");
            buffer.append(this.getTableName(managedBean));
            buffer.append(" AS ");
            buffer.append(quotedTableAlias);
        }
        return buffer.toString();
    }

    public String getCountAllSql(
            final ManagedBean managedBean, final String countAlias, final String tableAlias
    ) {
        final StringBuilder buffer = new StringBuilder("SELECT Count(*)");
        if ( null != countAlias && ! countAlias.isEmpty() ) {
            buffer.append(" AS ").append(quote(countAlias));
        }
        buffer.append(" FROM ").append(this.getTableNameWithAlias(managedBean, tableAlias));
        return buffer.toString();
    }

    public String getSelectPageSql(final ManagedBean managedBean, final String tableAlias) {
        return this.getSelectAllSql(managedBean, tableAlias) + " LIMIT ? OFFSET ?";
    }

    public String getSelectByIdSql(final ManagedBean managedBean, final String tableAlias) {
        return this.getSelectAllSql(managedBean, tableAlias) +
                " WHERE " + this.quoteColumn("id", tableAlias) + " = ?";
    }

    public String getSelectIdByIdInSql(
            final ManagedBean managedBean, final String idAlias, final String tableAlias, final int idCount
    ) {
        if (idCount <= 0) {
            throw new IllegalArgumentException("idCount <= 0: " + idCount);
        }

        final StringBuilder buffer = new StringBuilder("SELECT ");
        buffer.append(quote("id"));
        if ( null != idAlias && ! idAlias.isEmpty() ) {
            buffer.append(" AS ").append(quote(idAlias));
        }
        buffer.append(" FROM ").append(this.getTableNameWithAlias(managedBean, tableAlias));
        buffer.append(" WHERE ");
        buffer.append(this.quoteColumn("id", tableAlias)).append(" IN (");

        final StringBuilder temp = new StringBuilder();
        for (int i = 0; i < idCount; i += 1) {
            temp.append(',').append('?');
        }
        buffer.append(temp.substring(1));

        buffer.append(')');
        return buffer.toString();
    }

    public String getInsertSql(final ManagedBean managedBean) {
        final List<FieldOfBean> fieldList = managedBean.getFields();
        if (null == fieldList || fieldList.isEmpty()) {
            throw new IllegalArgumentException("The fields of the managedBean is null or empty");
        }

        final StringBuilder fields = new StringBuilder();
        final StringBuilder questionMarks = new StringBuilder();
        for (FieldOfBean field : fieldList) {
            fields.append(',').append(this.quote(field.getDbColumn()));
            questionMarks.append(',').append('?');
        }
        return "INSERT INTO " + this.getTableName(managedBean) +
                '(' + fields.substring(1) + ") VALUES (" +
                questionMarks.substring(1) + ')';
    }

    public String getUpdateByIdSql(final ManagedBean managedBean) {
        final List<FieldOfBean> fieldList = managedBean.getFields();
        if (null == fieldList || fieldList.isEmpty()) {
            throw new IllegalArgumentException("The fields of the managedBean is null or empty");
        }

        final StringBuilder fields = new StringBuilder();
        for (FieldOfBean field : fieldList) {
            if ( ! "id".equalsIgnoreCase(field.getName()) ) {
                fields.append(',').append(this.quote(field.getDbColumn())).append(" = ?");
            }
        }
        fields.setCharAt(0, ' ');
        return "UPDATE " + this.getTableName(managedBean) + " SET" +
                fields + " WHERE " + this.quote("id") + " = ?";
    }

    /**
     * Get the name of the specified data type in the database
     * @param dataType the data type defined in this framework
     * @return the data type name, null if the data type is not supported
     */
    public String getDataTypeName(final DataType dataType) {
        switch (dataType) {
            case INT32:
                return "INT";
            case INT64:
                return "BIGINT";
            case VARCHAR:
                return "VARCHAR";
        }
        return null;
    }

    public void setStatementParameter(final PreparedStatement statement, final int index,
            final DataType dataType, final Object value) throws SQLException {
        switch (dataType) {
            case INT32:
                if (null == value) {
                    statement.setNull(index, Types.INTEGER);
                } else if (value instanceof Number) {
                    statement.setInt(index, ((Number)value).intValue());
                } else {
                    statement.setInt(index, Integer.parseInt( value.toString() ));
                }
                break;
            case INT64:
                if (null == value) {
                    statement.setNull(index, Types.BIGINT);
                } else if (value instanceof Number) {
                    statement.setLong(index, ((Number)value).longValue());
                } else {
                    statement.setLong(index, Long.parseLong( value.toString() ));
                }
                break;
            case DATE_TIME:
                if (null == value) {
                    statement.setNull(index, Types.TIMESTAMP);
                } else if (value instanceof Number) {
                    statement.setTimestamp(index, new Timestamp( ((Number)value).longValue() ));
                } else if (value instanceof Date) {
                    statement.setTimestamp(index, new Timestamp( ((Date) value).getTime() ));
                } else {
                    throw new IllegalArgumentException("Cannot set a " +
                            value.getClass().getName() + " as a DATE_TIME parameter");
                }
                break;
            case DATE:
                if (null == value) {
                    statement.setNull(index, Types.DATE);
                } else if (value instanceof Number) {
                    statement.setDate(index, new java.sql.Date( ((Number)value).longValue() ));
                } else if (value instanceof Date) {
                    statement.setDate(index, new java.sql.Date( ((Date) value).getTime() ));
                } else {
                    throw new IllegalArgumentException("Cannot set a " +
                            value.getClass().getName() + " as a DATE parameter");
                }
                break;
            case TIME:
                if (null == value) {
                    statement.setNull(index, Types.TIME);
                } else if (value instanceof Number) {
                    statement.setTime(index, new Time( ((Number)value).longValue() ));
                } else if (value instanceof Date) {
                    statement.setTime(index, new Time( ((Date) value).getTime() ));
                } else {
                    throw new IllegalArgumentException("Cannot set a " +
                            value.getClass().getName() + " as a TIME parameter");
                }
                break;
            case CLOB:
                if (null == value) {
                    statement.setNull(index, Types.CLOB);
                } else if (value instanceof Clob) {
                    statement.setClob(index, (Clob) value);
                } else if (value instanceof char[]) {
                    statement.setClob(index, new SerialClob((char[]) value));
                } else {
                    statement.setClob(index, new SerialClob( value.toString().toCharArray() ));
                }
                break;
            case BLOB:
                if (null == value) {
                    statement.setNull(index, Types.BLOB);
                } else if (value instanceof Blob) {
                    statement.setBlob(index, (Blob) value);
                } else if (value instanceof byte[]) {
                    statement.setBlob(index, new SerialBlob((byte[]) value));
                }
                break;
            case UUID:
                if (this.getDataTypeName(DataType.UUID) != null) { // support UUID
                    if (null == value || value instanceof UUID) {
                        statement.setObject(index, value);
                    } else {
                        statement.setObject(index, UUID.fromString(value.toString()));
                    }
                    break;
                }
                // not support UUID, fallback to String
            default:
                if (null == value) {
                    statement.setNull(index, Types.VARCHAR);
                } else {
                    statement.setString(index, value.toString());
                }
                break;
        }
    }

    public abstract ResultSetRetriever getResultSetRetriever();

    protected static class QuestionMarkList implements List<Character> {
        private final int size;
        protected QuestionMarkList(final int size) {
            if (size <= 0) {
                throw new IllegalArgumentException("size must be a positive integer: " + size);
            }
            this.size = size;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public int size() {
            return this.size;
        }

        @Override
        public boolean contains(final Object o) {
            return Character.valueOf('?').equals(o);
        }

        @Override
        public Character get(final int index) {
            if (index >= this.size) {
                throw new IndexOutOfBoundsException(index + " >= " + this.size);
            }
            return '?';
        }

        @Override
        public Character set(final int index, final Character c) {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public boolean add(Character character) {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public void add(int index, Character element) {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public boolean addAll(Collection<? extends Character> c) {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public boolean addAll(int index, Collection<? extends Character> c) {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public Character remove(int index) {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Cannot write an immutable List");
        }

        @Override
        public int indexOf(Object o) {
            return this.contains(o) ? 0 : -1;
        }

        @Override
        public int lastIndexOf(Object o) {
            return this.contains(o) ? this.size - 1 : -1;
        }

        @Override
        public List<Character> subList(int fromIndex, int toIndex) {
            if (fromIndex < 0) {
                throw new IllegalArgumentException("fromIndex < 0: " + fromIndex);
            }
            if (toIndex < fromIndex) {
                throw new IllegalArgumentException("toIndex < fromIndex: " + toIndex + " < " + fromIndex);
            }
            if (toIndex >= this.size) {
                throw new IndexOutOfBoundsException("toIndex > " + this.size + ": " + toIndex);
            }
            return new QuestionMarkList(toIndex - fromIndex);
        }

        @Override
        public Object[] toArray() {
            final Object[] result = new Object[this.size];
            for (int i = 0; i < this.size; i += 1) {
                result[i] = '?';
            }
            return result;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T[] toArray(T[] a) {
            if (a.length < this.size) {
                return (T[]) this.toArray();
            }
            for (int i = 0; i < this.size; i += 1) {
                a[i] = (T) Character.valueOf('?');
            }
            if (a.length > this.size) {
                a[this.size] = null;
            }
            return a;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return c.stream().allMatch((item) -> Character.valueOf('?').equals(item));
        }

        @Override
        public Iterator<Character> iterator() {
            return new MyIterator();
        }

        @Override
        public ListIterator<Character> listIterator() {
            return new MyIterator();
        }

        @Override
        public ListIterator<Character> listIterator(final int index) {
            if (index > this.size) {
                throw new IndexOutOfBoundsException("index > " + this.size + ": " + index);
            }
            return new MyIterator(index);
        }

        private class MyIterator implements ListIterator<Character> {
            private int index;
            private MyIterator() {
                this.index = 0;
            }
            private MyIterator(final int index) {
                this.index = index;
            }

            @Override
            public boolean hasNext() {
                return this.index < size;
            }

            @Override
            public Character next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                this.index += 1;
                return '?';
            }

            @Override
            public boolean hasPrevious() {
                return this.index > 0;
            }

            @Override
            public Character previous() {
                if (!hasPrevious()) {
                    throw new NoSuchElementException();
                }
                this.index -= 1;
                return '?';
            }

            @Override
            public int nextIndex() {
                return this.index;
            }

            @Override
            public int previousIndex() {
                return this.index - 1;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Cannot write an immutable List");
            }

            @Override
            public void set(Character character) {
                throw new UnsupportedOperationException("Cannot write an immutable List");
            }

            @Override
            public void add(Character character) {
                throw new UnsupportedOperationException("Cannot write an immutable List");
            }
        }
    }
}
