package tech.firas.framework.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import javax.validation.ValidationException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import tech.firas.framework.dialect.SqlDialect;
import tech.firas.framework.dialect.resultset.ResultSetRetriever;
import tech.firas.framework.event.EventManager;
import tech.firas.framework.po.FieldOfBean;
import tech.firas.framework.po.IdModel;
import tech.firas.framework.po.ManagedBean;
import tech.firas.framework.po.ManagedBeanLog;
import tech.firas.framework.po.PermissionBase;
import tech.firas.framework.po.ValidationRule;
import tech.firas.framework.pojo.DataType;
import tech.firas.framework.pojo.ManagedBeanWithPermissionType;
import tech.firas.framework.pojo.MyPage;
import tech.firas.framework.pojo.MyPageRequest;
import tech.firas.framework.repository.ManagedBeanRepository;
import tech.firas.framework.repository.UserRepository;
import tech.firas.framework.validator.Validator;

@lombok.extern.slf4j.Slf4j
@org.springframework.stereotype.Service
public class ManagedBeanServiceImpl implements ManagedBeanService {

    @Override
    @PreAuthorize("hasAnyAuthority('VIEWER:00000000-0000-0000-0000-000000000000', " +
            "'EDITOR:00000000-0000-0000-0000-000000000000', " +
            "'ADMIN:00000000-0000-0000-0000-000000000000', " +
            "'OWNER:00000000-0000-0000-0000-000000000000')")
    public MyPage<ManagedBean> listByPage(final MyPageRequest myPageRequest) {
        if (log.isDebugEnabled()) log.debug( String.valueOf(myPageRequest) );

        PageRequest pageRequest = PageRequest.of(myPageRequest.getPage(), myPageRequest.getSize());
        return new MyPage<>(this.managedBeanRepository.findAll(pageRequest));
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("hasAnyAuthority('VIEWER:00000000-0000-0000-0000-000000000000', " +
            "'EDITOR:00000000-0000-0000-0000-000000000000', " +
            "'ADMIN:00000000-0000-0000-0000-000000000000', " +
            "'OWNER:00000000-0000-0000-0000-000000000000')")
    public Optional<ManagedBean> getById(final UUID id) {
        return this.managedBeanRepository.findById(id).map(managedBean -> {
            final List<FieldOfBean> fields = this.fieldOfBeanService.listFieldsOfBean(managedBean);
            log.debug("Number of fields: " + fields.size());
            managedBean.setFields(fields);
            return managedBean;
        });
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public ArrayList<ManagedBeanWithPermissionType> listPermittedBeans() {
        final Map<UUID, String> beanIdToPermissionTypeMap = new HashMap<>();
        final List<UUID> beanIdList = SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .map(GrantedAuthority::getAuthority).map(authority -> {
                    final String[] splitted = authority.split(":", 2);
                    final UUID beanId = UUID.fromString(splitted[1]);
                    beanIdToPermissionTypeMap.put(beanId, splitted[0]);
                    return beanId;
                }).collect(Collectors.toList());
        return this.managedBeanRepository.findAllById(beanIdList).stream()
                .map(bean -> {
                    final UUID beanId = bean.getId();
                    return ManagedBeanWithPermissionType.of(bean,
                            PermissionBase.PermissionType.valueOf(beanIdToPermissionTypeMap.get(beanId)));
                }).collect(() -> new ArrayList<>(beanIdList.size()), List::add, List::addAll);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    @PreAuthorize("hasAnyAuthority('EDITOR:00000000-0000-0000-0000-000000000000', " +
            "'ADMIN:00000000-0000-0000-0000-000000000000', " +
            "'OWNER:00000000-0000-0000-0000-000000000000')")
    public ManagedBean insertOrUpdate(final ManagedBean managedBean) throws Exception {
        Optional<ManagedBean> beanOptional = getByName(managedBean.getName());
        if ( beanOptional.isPresent() && ! beanOptional.get().getId().equals(managedBean.getId()) ) {
            throw new ValidationException(ErrorCode.BEAN_NAME_CONFLICT.getCode());
        }

        final List<FieldOfBean> fields = Optional.ofNullable( managedBean.getFields() )
                .orElse( new ArrayList<>() ); // fields to save
        validateFieldNames(fields);

        if (managedBean.getId() == null) {
            return this.insert(managedBean, fields);
        }

        beanOptional = getById(managedBean.getId());
        if (!beanOptional.isPresent()) {
            return this.insert(managedBean, fields);
        }

        final Map<UUID, FieldOfBean> fieldMap = fields.stream()
                .filter( (field) -> field.getId() != null )
                .collect( HashMap::new, (map, field) -> map.put(field.getId(), field), Map::putAll );

        this.filterIdOfFieldOfOtherBean(managedBean, fieldMap);
        this.removeFieldOfBeanWhoseIdNotIn(managedBean, fieldMap);

        final ManagedBean saved = this.doSave(managedBean, fields);

        final ManagedBeanLog log = ofBeanLog(beanOptional.get(), false);
        this.eventManager.triggerEvent(Event.BEAN_UPDATED.getEventName(), log);
        return saved;
    }

    private ManagedBean insert(final ManagedBean managedBean, final List<FieldOfBean> fields)
            throws JsonProcessingException {
        fields.forEach((field) -> field.setId(null));
        final ManagedBean saved = this.doSave(managedBean, fields);

        final ManagedBeanLog log = ofBeanLog(managedBean, true);
        this.eventManager.triggerEvent(Event.BEAN_CREATED.getEventName(), log);
        return saved;
    }

    private ManagedBean doSave(final ManagedBean managedBean, final List<FieldOfBean> fields) {
        final ManagedBean saved = this.managedBeanRepository.save(managedBean);
        log.debug(String.valueOf(saved == managedBean));

        for (FieldOfBean field : fields) {
            field.setBean(saved);
        }

        saved.setFields(this.fieldOfBeanService.saveAll(fields));

        return saved;
    }

    private void filterIdOfFieldOfOtherBean(final ManagedBean saved, final Map<UUID, FieldOfBean> fieldMap) {
        final List<FieldOfBean> cannotSaveFields = this.fieldOfBeanService.listFieldsNotOfBeanByIds(saved, fieldMap.keySet());
        cannotSaveFields.forEach((cannotSaveField) -> {
            final FieldOfBean temp = fieldMap.get(cannotSaveField.getId());
            if (null != temp) {
                temp.setId(null);
                fieldMap.remove(cannotSaveField.getId());
            }
        });
    }

    private void removeFieldOfBeanWhoseIdNotIn(final ManagedBean saved, final Map<UUID, FieldOfBean> fieldMap) {
        final Set<UUID> toRemoveSet = this.fieldOfBeanService.listFieldsOfBean(saved).stream() // original fields
                .map(FieldOfBean::getId)
                .peek( (id) -> log.debug(String.valueOf(id)) )
                .filter( (id) -> !fieldMap.containsKey(id) )
                .peek( (id) -> log.debug("toRemove: " + id) )
                .collect( HashSet::new, Set::add, Set::addAll );
        if ( ! toRemoveSet.isEmpty() ) {
            this.fieldOfBeanService.deleteByIdCollection(toRemoveSet);
        }
    }

    private static void validateFieldNames(final List<FieldOfBean> fields) {
        final Set<String> fieldNames = new HashSet<>();
        for (final FieldOfBean field : fields) {
            if (null == field.getName() || field.getName().trim().isEmpty()) {
                throw new ValidationException(ErrorCode.FIELD_NAME_BLANK.getCode());
            }
            if ( fieldNames.contains(field.getName()) ) {
                throw new ValidationException(ErrorCode.FIELD_NAME_CONFLICT.getCode());
            }
            fieldNames.add(field.getName());
        }
    }

    @Override
    //@PreAuthorize("hasAnyAuthority('VIEWER:'+#name, 'EDITOR:'+#name, 'ADMIN:'+#name, 'OWNER:'+#name)")
    public Optional<ManagedBean> getByName(final String name) {
        return this.managedBeanRepository.findOne(
                (root, query, builder) -> builder.equal(root.get("name"), name)
        ).map(managedBean -> {
            final List<FieldOfBean> fields = this.fieldOfBeanService.listFieldsOfBean(managedBean);
            log.debug("Number of fields: " + fields.size());
            managedBean.setFields(fields);
            return managedBean;
        });
    }

    @Override
    @PreAuthorize("hasAnyAuthority('VIEWER:'+#managedBean.id, 'EDITOR:'+#managedBean.id, " +
            "'ADMIN:'+#managedBean.id, 'OWNER:'+#managedBean.id)")
    public Iterator<Map<String, Object>> listAllBean(final ManagedBean managedBean) throws Exception {
        final Connection connection = this.dataSource.getConnection();
        final Statement statement = connection.createStatement();
        final ResultSet resultSet = statement.executeQuery( this.sqlDialect.getSelectAllSql(managedBean, "") );
        return new BeanIterator(this.sqlDialect.getResultSetRetriever(), managedBean.getFields(), resultSet);
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("hasAnyAuthority('VIEWER:'+#managedBean.id, 'EDITOR:'+#managedBean.id, " +
            "'ADMIN:'+#managedBean.id, 'OWNER:'+#managedBean.id)")
    public Page<Map<String, Object>> listBeanByPage(final ManagedBean managedBean, final PageRequest pageRequest)
            throws Exception {
        final long total;
        final List<Map<String, Object>> content;

        try (final Connection connection = this.dataSource.getConnection()) {
            try (final Statement statement = connection.createStatement()) {
                try (final ResultSet resultSetTotal = statement.executeQuery(this.sqlDialect.getCountAllSql(
                        managedBean, "", ""))) {
                    resultSetTotal.next();
                    total = resultSetTotal.getLong(1);
                    if (log.isDebugEnabled()) log.debug("total: " + total);
                }
            }

            final PreparedStatement statement = connection.prepareStatement(this.sqlDialect.getSelectPageSql(
                    managedBean, ""));
            statement.setInt(1, pageRequest.getPageSize());
            statement.setLong(2, pageRequest.getOffset());
            final ResultSet resultSet = statement.executeQuery();
            final Iterator<Map<String, Object>> iterator = new BeanIterator(
                    this.sqlDialect.getResultSetRetriever(), managedBean.getFields(), resultSet);

            content = new ArrayList<>(pageRequest.getPageSize());
            while (iterator.hasNext()) {
                content.add(iterator.next());
            }
        }
        return new PageImpl<>(content, pageRequest, total);
    }

    @Override
    @PreAuthorize("hasAnyAuthority('VIEWER:'+#managedBean.id, 'EDITOR:'+#managedBean.id, " +
            "'ADMIN:'+#managedBean.id, 'OWNER:'+#managedBean.id)")
    public Map<String, Object> getBeanById(final ManagedBean managedBean, final UUID id) throws Exception {
        try (final Connection connection = this.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                this.sqlDialect.getSelectByIdSql(managedBean, "")
            )) {
                preparedStatement.setString(1, id.toString());
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    return resultSet.next() ? getBeanFromResultSet(
                            this.sqlDialect.getResultSetRetriever(), managedBean.getFields(), resultSet) : null;
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    @PreAuthorize("hasAnyAuthority('EDITOR:'+#managedBean.id, " +
            "'ADMIN:'+#managedBean.id, 'OWNER:'+#managedBean.id)")
    public Map<String, Object> insertOrUpdateBean(final ManagedBean managedBean,
            final Map<String, Object> bean) throws Exception {
        Object id = bean.get("id");
        if (id instanceof String) {
            id = UUID.fromString((String) id);
        }
        if (id != null) {
            if (!(id instanceof UUID)) {
                throw new IllegalArgumentException(ErrorCode.BEAN_ID_INVALID.getCode());
            }
            this.validateBean(managedBean, bean);
            final Map<String, Object> existOne = this.getBeanById(managedBean, (UUID) id);
            if (null != existOne) {
                bean.put("createTimestamp", existOne.get("createTimestamp"));
                return updateBeans(managedBean, Collections.singletonList(bean)).get(0);
            }
        }

        return insertBeans(managedBean, Collections.singletonList(bean)).get(0);
    }

    private void validateBean(final ManagedBean managedBean, final Map<String, Object> bean) {
        final Set<UUID> fieldIds = managedBean.getFields().stream().collect(HashSet::new,
                (idSet, field) -> idSet.add(field.getId()), Set::addAll);
        final Map<UUID, List<ValidationRule>> ruleMap = this.validationRuleServiceImpl.getRuleMapByFieldIds(fieldIds);
        for (final FieldOfBean field : managedBean.getFields()) {
            final List<ValidationRule> rules = ruleMap.get(field.getId());
            if (rules == null) {
                continue;
            }
            for (final ValidationRule rule : rules) {
                final Object value = bean.get(field.getName());
                Validator.validate(rule, value == null ? null : value.toString());
            }
        }
    }

    private List<Map<String, Object>> insertBeans(
            final ManagedBean managedBean,
            final List<Map<String, Object>> beans
    ) throws SQLException {
        final String sql = this.sqlDialect.getInsertSql(managedBean);

        try (final Connection connection = this.dataSource.getConnection()) {
            try (final PreparedStatement ps = connection.prepareStatement(sql)) {
                final long currentTimestamp = System.currentTimeMillis();
                for (final Map<String, Object> bean : beans) {
                    bean.put("id", UUID.randomUUID());
                    bean.put("createTimestamp", currentTimestamp);
                    bean.put("updateTimestamp", currentTimestamp);

                    final int insertedRowCount = setStatementParametersAndExecute(ps, bean, managedBean.getFields());
                    if (insertedRowCount != 1) {
                        log.warn("The number of the row inserted is not 1 {\"beanName\":\"" +
                                managedBean.getName() + "\",\"id\":\"" + bean.get("id") +
                                "\",\"insertedRowCount\":" + insertedRowCount + '}');
                    }
                }
            }
        }
        return beans;
    }

    private List<Map<String, Object>> updateBeans(final ManagedBean managedBean, final List<Map<String, Object>> beans)
            throws SQLException {
        final String sql = this.sqlDialect.getUpdateByIdSql(managedBean);

        final List<FieldOfBean> fields = managedBean.getFields().stream()
                .filter( (field) -> !"id".equals(field.getName()) )
                .collect(Collectors.toList());
        fields.add(getIdField(managedBean));

        try (final Connection connection = this.dataSource.getConnection()) {
            try (final PreparedStatement ps = connection.prepareStatement(sql)) {
                final long currentTimestamp = System.currentTimeMillis();
                for (final Map<String, Object> bean : beans) {
                    bean.put("updateTimestamp", currentTimestamp);

                    final int updatedRowCount = setStatementParametersAndExecute(ps, bean, fields);
                    if (updatedRowCount != 1) {
                        log.warn("The number of the row updated is not 1 {\"beanName\":\"" +
                                managedBean.getName() + "\",\"id\":\"" + bean.get("id") +
                                "\",\"updatedRowCount\":" + updatedRowCount + '}');
                    }
                }
            }
        }
        return beans;
    }

    private static FieldOfBean getIdField(final ManagedBean managedBean) {
        final FieldOfBean result = new FieldOfBean();
        result.setDataType(DataType.UUID);
        result.setName("id");
        result.setDbColumn("id");
        result.setBean(managedBean);
        return result;
    }

    private int setStatementParametersAndExecute(
            final PreparedStatement ps,
            final Map<String, Object> bean,
            final List<FieldOfBean> fields
    ) throws SQLException{
        int i = 1;
        for (final FieldOfBean field : fields) {
            final Object fieldValue = bean.get(field.getName());
            if (isDateTimeType(field.getDataType()) &&
                    fieldValue != null &&
                    !(fieldValue instanceof Number) &&
                    field.getDateTimePattern() != null) {
                final DateTimeFormatter format = DateTimeFormatter.ofPattern(field.getDateTimePattern());
                Instant instant = Instant.from( format.parse(fieldValue.toString()) );
                this.sqlDialect.setStatementParameter(ps, i++, field.getDataType(), new Date(instant.toEpochMilli()) );
            } else {
                this.sqlDialect.setStatementParameter(ps, i++, field.getDataType(), fieldValue);
            }
        }
        return ps.executeUpdate();
    }

    private static boolean isDateTimeType(final DataType dataType) {
        return DataType.DATE_TIME.equals(dataType) ||
                DataType.DATE.equals(dataType) ||
                DataType.TIME.equals(dataType);
    }

    /*
    @Transactional(rollbackFor = {Exception.class}, propagation = Propagation.REQUIRED)
    public Map< String, List<Map<String, Object>> > insertOrUpdateBeans(Map< String, List<Map<String, Object>> > beans) {
        final Map<String, ManagedBean> beanMap = this.findByIdCollection(beans.keySet()).stream().collect(
                HashMap::new, (map, bean) -> map.put(bean.getId().toString(), bean), Map::putAll
        );
        for (final String beanId : beans.keySet()) {
            if ( ! beanMap.containsKey(beanId) ) {
                throw new IllegalArgumentException(ErrorCode.BEAN_ID_NOT_EXIST.getCode());
            }
        }
        for (final Map.Entry< String, List<Map<String, Object>> > entry : beans.entrySet()) {
            final String beanId = entry.getKey();
            final ManagedBean managedBean = beanMap.get(beanId);
            final List<Map<String, Object>> beansToSave = entry.getValue();
            //
        }
        return null;
    }

    private List<Map<String, Object>> saveBeans(
            final ManagedBean managedBean,
            final List<Map<String, Object>> beansToSave
    ) throws SQLException {
        Set<String> idsToCheck = beansToSave.stream()
                .map((bean) -> bean.get("id"))
                .filter(Objects::nonNull)
                .map(Object::toString)
                .collect(Collectors.toSet());

        try (Connection connection = this.dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery())
            }
        }
        final List<FieldOfBean> fields = managedBean.getFields();

        beansToSave.stream().map( (bean) -> {
                saveBean(managedBean.getDbSchema(), managedBean.getDbTable(), fields)
        } );
    }
    */

    private List<ManagedBean> findByIdCollection(final Collection<String> idCollection) {
        return this.managedBeanRepository.findAll( (root, query, builder) ->
                root.get("id").in( idCollection.stream().map(UUID::fromString).collect(Collectors.toList()) )
        );
    }

    private ManagedBeanLog ofBeanLog(final ManagedBean managedBean, final boolean isCreate)
            throws JsonProcessingException{
        String ip = this.request.getHeader("x-forwarded-for");
        if (null == ip || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = this.request.getRemoteAddr();
        } else if (ip.contains(",")) {
            ip = ip.split(",", 2)[0];
        }

        final String userAgent = this.request.getHeader(HttpHeaders.USER_AGENT);

        final Optional<UUID> userId = Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .map(Authentication::getPrincipal)
                .map(detail -> detail instanceof String ? (String) detail :
                        (detail instanceof UserDetails) ? ((UserDetails) detail).getUsername() : null)
                .map(username -> this.userRepository.getFirstByUsername(username))
                .map(IdModel::getId);

        return new ManagedBeanLog(ManagedBeanLog.Type.ManagedBean, managedBean.getId(),
                jsonConverter.writerWithView(ManagedBean.ViewWithFields.class).writeValueAsString(managedBean),
                isCreate, ip, userAgent, userId.orElse(null));
    }

    private DataSource dataSource;
    @Inject
    public void setDataSource(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private EventManager<Object> eventManager;
    @Inject
    public void setEventManager(final EventManager<Object> eventManager) {
        this.eventManager = eventManager;
    }

    private ObjectMapper jsonConverter;
    @Inject
    public void setJsonConverter(final ObjectMapper jsonConverter) {
        this.jsonConverter = jsonConverter;
    }

    private HttpServletRequest request;
    @Inject
    public void setRequest(final HttpServletRequest request) {
        this.request = request;
    }

    private ManagedBeanRepository managedBeanRepository;
    @Inject
    public void setManagedBeanRepository(final ManagedBeanRepository managedBeanRepository) {
        this.managedBeanRepository = managedBeanRepository;
    }

    private FieldOfBeanService fieldOfBeanService;
    @Inject
    public void setFieldOfBeanService(final FieldOfBeanService fieldOfBeanService) {
        this.fieldOfBeanService = fieldOfBeanService;
    }

    private ValidationRuleService validationRuleServiceImpl;
    @Inject
    public void setValidationRuleServiceImpl(final ValidationRuleService validationRuleServiceImpl) {
        this.validationRuleServiceImpl = validationRuleServiceImpl;
    }

    private UserRepository userRepository;
    @Inject
    public void setUserRepository(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private SqlDialect sqlDialect;
    public void setSqlDialect(final SqlDialect sqlDialect) {
        this.sqlDialect = sqlDialect;
    }

    private static final class BeanIterator implements Iterator<Map<String, Object>> {
        private final ResultSetRetriever retriever;
        private final Collection<FieldOfBean> fields;
        private final ResultSet resultSet;
        private Boolean cachedHasNext;

        private BeanIterator(final ResultSetRetriever retriever,
                final Collection<FieldOfBean> fields, final ResultSet resultSet) {
            this.retriever = retriever;
            this.fields = fields;
            this.resultSet = resultSet;
            this.cachedHasNext = null;
        }

        @Override public boolean hasNext() {
            if (null == this.cachedHasNext) {
                try {
                    this.cachedHasNext = this.resultSet.next();
                } catch (final SQLException ex) {
                    throw new RuntimeException("Fail to tranverse the ResultSet", ex);
                }
                if (!this.cachedHasNext) {
                    this.closeResultSet();
                }
            }
            return this.cachedHasNext;
        }

        @Override public Map<String, Object> next() {
            if (null == this.cachedHasNext) {
                try {
                    this.resultSet.next();
                } catch (final SQLException ex) {
                    throw new RuntimeException("Fail to tranverse the ResultSet", ex);
                }
            } else if (Boolean.FALSE.equals(this.cachedHasNext)) {
                throw new NoSuchElementException();
            }

            this.cachedHasNext = null;
            try {
                return getBeanFromResultSet(this.retriever, this.fields, this.resultSet);
            } catch (final SQLException ex) {
                throw new RuntimeException("Fail to tranverse the ResultSet", ex);
            }
        }

        private void closeResultSet() {
            try {
                Statement statement = this.resultSet.getStatement();
                Connection connection = statement.getConnection();

                try {
                    this.resultSet.close();
                } catch (final SQLException e) {
                    log.error("Fail to close the ResultSet", e);
                }
                try {
                    statement.close();
                } catch (SQLException e) {
                    log.error("Fail to close the Statement", e);
                }
                try {
                    connection.close();
                } catch (final SQLException e) {
                    log.error("Fail to close the Connection", e);
                }
            } catch (final SQLException ex) {
                log.error("Fail to get the Connection of the ResultSet", ex);
            }
        }
    }

    private static Map<String, Object> getBeanFromResultSet(final ResultSetRetriever retriever,
            final Collection<FieldOfBean> fields, final ResultSet resultSet) throws SQLException {
        final Map<String, Object> result = new LinkedHashMap<>(fields.size());
        for (final FieldOfBean field : fields) {
            switch (field.getDataType()) {
                case INT32:
                    result.put(field.getName(), retriever.getInteger(resultSet, field.getDbColumn()));
                    break;
                case INT64:
                    result.put(field.getName(), retriever.getLong(resultSet, field.getDbColumn()));
                    break;
                case DATE_TIME:
                    final Timestamp timestamp = retriever.getDateTime(resultSet, field.getDbColumn());
                    if (null == timestamp) {
                        result.put(field.getName(), null);
                        break;
                    }
                    if ( field.getDateTimePattern() != null && ! field.getDateTimePattern().isEmpty() ) {
                        final String value = DateTimeFormatter.ofPattern(field.getDateTimePattern()).format(
                                Instant.ofEpochMilli(timestamp.getTime())
                        );
                        result.put(field.getName(), value);
                    } else {
                        result.put(field.getName(), timestamp.getTime());
                    }
                    break;
                case DATE:
                    final Date date = retriever.getDate(resultSet, field.getDbColumn());
                    if (null == date) {
                        result.put(field.getName(), null);
                        break;
                    }
                    if ( field.getDateTimePattern() != null && ! field.getDateTimePattern().isEmpty() ) {
                        final String value = DateTimeFormatter.ofPattern(field.getDateTimePattern()).format(
                                Instant.ofEpochMilli(date.getTime())
                        );
                        result.put(field.getName(), value);
                    } else {
                        result.put(field.getName(), date.getTime());
                    }
                    break;
                case TIME:
                    final Time time = retriever.getTime(resultSet, field.getDbColumn());
                    if (null == time) {
                        result.put(field.getName(), null);
                        break;
                    }
                    if ( field.getDateTimePattern() != null && ! field.getDateTimePattern().isEmpty() ) {
                        final String value = DateTimeFormatter.ofPattern(field.getDateTimePattern()).format(
                                Instant.ofEpochMilli(time.getTime())
                        );
                        result.put(field.getName(), value);
                    } else {
                        result.put(field.getName(), time.getTime());
                    }
                    break;
                default:
                    result.put( field.getName(), resultSet.getString(field.getDbColumn()) );
            }
        }
        return result;
    }

    private static final Map<String, DateFormat> defaultDateTimeFormatterCache = new HashMap<>(4);
    static {
        defaultDateTimeFormatterCache.put("yyyy-MM-dd", new SimpleDateFormat("yyyy-MM-dd"));
        defaultDateTimeFormatterCache.put("yyyy-MM-dd HH:mm:ss", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        defaultDateTimeFormatterCache.put("yyyy-MM-dd HH:mm:ss.SSS", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));
    }
}
