package tech.firas.framework.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;

import tech.firas.framework.po.FieldOfBean;
import tech.firas.framework.po.ManagedBean;
import tech.firas.framework.repository.FieldOfBeanRepository;

@lombok.extern.slf4j.Slf4j
@org.springframework.stereotype.Service
public class FieldOfBeanServiceImpl implements FieldOfBeanService {

    @Override
    public List<FieldOfBean> listFieldsOfBean(ManagedBean managedBean) {
        return this.fieldOfBeanRepository.findAll(
                (root, query, builder) -> builder.equal(root.get("bean").get("id"), managedBean.getId())
        );
    }

    @Override
    public List<FieldOfBean> listFieldsNotOfBeanByIds(ManagedBean managedBean, Collection<UUID> ids) {
        if (null == ids || ids.isEmpty()) {
            return emptyFieldList;
        }
        return this.fieldOfBeanRepository.findAll( (root, query, builder) ->
                builder.and( builder.notEqual(root.get("bean").get("id"), managedBean.getId()),
                        root.get("id").in(ids) )
        );
    }

    @Override
    public List<FieldOfBean> saveAll(Collection<FieldOfBean> fields) {
        return this.fieldOfBeanRepository.saveAll(fields);
    }

    @Override
    public void deleteByIdCollection(Collection<UUID> idCollection) {
        this.fieldOfBeanRepository.deleteByIdCollection(idCollection);
    }

    private FieldOfBeanRepository fieldOfBeanRepository;
    @Inject
    public void setFieldOfBeanRepository(FieldOfBeanRepository fieldOfBeanRepository) {
        this.fieldOfBeanRepository = fieldOfBeanRepository;
    }

    private static final List<FieldOfBean> emptyFieldList = Collections.unmodifiableList(new ArrayList<>());
}
