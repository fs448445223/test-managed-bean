package tech.firas.framework.service;

/**
 * The rule of code:
 * [ServiceName]:[error.fieldName.beanName]
 */
public interface ServiceErrorCode {
    String getCode();
    boolean equalsCode(String str);
}
