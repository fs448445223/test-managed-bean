package tech.firas.framework.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import tech.firas.framework.po.ValidationRule;
import tech.firas.framework.repository.ValidationRuleRepository;
import org.springframework.stereotype.Service;

@Service
public class ValidationRuleServiceImpl implements ValidationRuleService {

    @Override
    public Map<UUID, List<ValidationRule>> getRuleMapByFieldIds(Collection<UUID> fieldIds) {
        return this.validationRuleRepository.findAll(
                (root, query, builder) -> root.get("field").get("id").in(fieldIds)
        ).stream().collect(
                HashMap::new,
                (map, rule) -> map.computeIfAbsent(rule.getField().getId(), (key) -> new ArrayList<>()).add(rule),
                Map::putAll
        );
    }

    private ValidationRuleRepository validationRuleRepository;
    @Inject
    public void setValidationRuleRepository(ValidationRuleRepository validationRuleRepository) {
        this.validationRuleRepository = validationRuleRepository;
    }
}
