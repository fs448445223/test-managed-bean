package tech.firas.framework.service;

import javax.inject.Inject;

import org.springframework.beans.factory.InitializingBean;

import tech.firas.framework.event.EventManager;
import tech.firas.framework.po.LogModel;
import tech.firas.framework.repository.ManagedBeanRepository;
import tech.firas.framework.repository.RolePermissionRepository;

@lombok.extern.slf4j.Slf4j
@org.springframework.stereotype.Service
public class RolePermissionServiceImpl implements RolePermissionService, InitializingBean {

    @Override
    public void afterPropertiesSet() {
        this.eventManager.addEventListener(ManagedBeanService.Event.BEAN_DELETED.getEventName(), obj -> {
            if (!(obj instanceof LogModel)) {
                throw new IllegalArgumentException("The parameter is not a LogModel");
            }
            final LogModel log = (LogModel) obj;
            this.rolePermissionRepository.deleteByBeanId(log.getModelId());
        });
    }

    private EventManager<Object> eventManager;
    @Inject
    public void setEventManager(final EventManager<Object> eventManager) {
        this.eventManager = eventManager;
    }

    private RolePermissionRepository rolePermissionRepository;
    @Inject
    public void setRolePermissionRepository(final RolePermissionRepository rolePermissionRepository) {
        this.rolePermissionRepository = rolePermissionRepository;
    }

    private ManagedBeanRepository managedBeanRepository;
    @Inject
    public void setManagedBeanRepository(final ManagedBeanRepository managedBeanRepository) {
        this.managedBeanRepository = managedBeanRepository;
    }
}
