package tech.firas.framework.service;

import javax.inject.Inject;

import org.springframework.beans.factory.InitializingBean;

import tech.firas.framework.event.EventManager;
import tech.firas.framework.po.ManagedBeanLog;
import tech.firas.framework.repository.ManagedBeanLogRepository;

@lombok.extern.slf4j.Slf4j
@org.springframework.stereotype.Service
public class ManagedBeanLogServiceImpl implements ManagedBeanLogService, InitializingBean {

    @Override
    public void afterPropertiesSet() {
        for (final ManagedBeanService.Event event : ManagedBeanService.Event.values()) {
            this.eventManager.addEventListener(event.getEventName(), obj -> {
                if (!(obj instanceof ManagedBeanLog)) {
                    throw new IllegalArgumentException("The parameter is not a LogModel");
                }
                this.managedBeanLogRepository.save((ManagedBeanLog) obj);
            });
        }
    }

    private EventManager<Object> eventManager;
    @Inject
    public void setEventManager(final EventManager<Object> eventManager) {
        this.eventManager = eventManager;
    }

    private ManagedBeanLogRepository managedBeanLogRepository;
    @Inject
    public void setManagedBeanLogRepository(final ManagedBeanLogRepository managedBeanLogRepository) {
        this.managedBeanLogRepository = managedBeanLogRepository;
    }
}
