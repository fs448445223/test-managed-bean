package tech.firas.framework.service;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;

import tech.firas.framework.po.ManagedBean;
import tech.firas.framework.pojo.ManagedBeanWithPermissionType;
import tech.firas.framework.pojo.MyPage;
import tech.firas.framework.pojo.MyPageRequest;

@Validated
public interface ManagedBeanService {

    enum Event implements ServiceEvent {
        BEAN_CREATED("ManagedBean.created"),
        BEAN_UPDATED("ManagedBean.updated"),
        BEAN_DELETED("ManagedBean.deleted");

        private final String eventName;
        Event(final String eventName) {
            this.eventName = eventName;
        }

        @Override
        public String getEventName() {
            return this.eventName;
        }
    }

    enum ErrorCode implements ServiceErrorCode {
        BEAN_ID_NOT_EXIST("ManagedBean:notExist.id.bean"),
        BEAN_ID_INVALID("ManagedBean:invalid.id.bean"),
        BEAN_NAME_NOT_EXIST("ManagedBean:notExist.name.bean"),
        BEAN_NAME_CONFLICT("ManagedBean:conflict.name.bean"),
        FIELD_NAME_CONFLICT("ManagedBean:conflict.name.field"),
        FIELD_NAME_BLANK("ManagedBean:notBlank.name.field"),
        UNKNOWN("ManagedBean:unknown");

        private final String code;
        ErrorCode(final String code) {
            this.code = code;
        }

        @Override
        public String getCode() {
            return this.code;
        }

        @Override
        public boolean equalsCode(final String code) {
            return this.code.equals(code);
        }
    }

    MyPage<ManagedBean> listByPage(@NotNull @Valid MyPageRequest myPageRequest);

    <T extends Serializable & List<ManagedBeanWithPermissionType>> T listPermittedBeans();

    ManagedBean insertOrUpdate(@NotNull @Valid ManagedBean managedBean) throws Exception;

    Optional<ManagedBean> getById(@NotNull UUID id);
    Optional<ManagedBean> getByName(@NotNull String name);

    Iterator<Map<String, Object>> listAllBean(@NotNull @Valid ManagedBean managedBean) throws Exception;

    Page<Map<String, Object>> listBeanByPage(
            @NotNull @Valid ManagedBean managedBean,
            @NotNull @Valid PageRequest pageRequest
    ) throws Exception;

    Map<String, Object> getBeanById(@NotNull @Valid ManagedBean managedBean, @NotNull UUID id) throws Exception;

    Map<String, Object> insertOrUpdateBean(
            @NotNull @Valid ManagedBean managedBean,
            @NotNull Map<String, Object> bean
    ) throws Exception;
}
