package tech.firas.framework.service;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import tech.firas.framework.po.FieldOfBean;
import tech.firas.framework.po.ManagedBean;

public interface FieldOfBeanService {

    List<FieldOfBean> listFieldsOfBean(ManagedBean managedBean);

    List<FieldOfBean> listFieldsNotOfBeanByIds(ManagedBean managedBean, Collection<UUID> ids);

    List<FieldOfBean> saveAll(Collection<FieldOfBean> fields);

    void deleteByIdCollection(Collection<UUID> idCollection);
}
