package tech.firas.framework.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import tech.firas.framework.pojo.ClientInfo;
import tech.firas.framework.pojo.MyAuthenticationToken;
import tech.firas.framework.pojo.MyResponse;

@lombok.extern.slf4j.Slf4j
@org.springframework.stereotype.Service
public class UsernamePasswordAuthenticationProvider implements AuthenticationProvider,
        AuthenticationSuccessHandler, AuthenticationFailureHandler, LogoutSuccessHandler, AuthenticationEntryPoint,
        AuthenticationDetailsSource<HttpServletRequest, ClientInfo> {

    private static final ObjectMapper jsonMapper = new ObjectMapper();

    private UserDetailsService userDetailsServiceImpl = null;
    private PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    @Autowired
    public void setUserDetailsServiceImpl(final UserDetailsService userDetailsServiceImpl) {
        this.userDetailsServiceImpl = userDetailsServiceImpl;
    }

    /* AuthenticationProvider */
    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        final UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
        final String username = (String) token.getPrincipal();
        final UserDetails userDetails = this.userDetailsServiceImpl.loadUserByUsername(username);
        if (!this.passwordEncoder.matches((String) token.getCredentials(), userDetails.getPassword())) {
            throw new BadCredentialsException("Bad credentials");
        }

        if (!userDetails.isAccountNonLocked()) {
            throw new LockedException("The user is locked: " + username);
        }
        if (!userDetails.isAccountNonExpired()) {
            throw new AccountExpiredException("The user is expired: " + username);
        }
        if (!userDetails.isCredentialsNonExpired()) {
            throw new CredentialsExpiredException("The password of the user is expired: " + username);
        }

        return new MyAuthenticationToken(username, userDetails.getAuthorities());
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    /* AuthenticationEntryPoint */
    @Override
    public void commence(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse,
            final AuthenticationException e) {
        final MyResponse<String> response = new MyResponse<>(ErrorCode.NEED_RE_LOGIN, "需要重新登录", null);
        responseJson(httpServletResponse, response);
    }

    /* AuthenticationFailureHandler */
    @Override
    public void onAuthenticationFailure(final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final AuthenticationException exception) {
        final MyResponse result;
        if (exception instanceof BadCredentialsException || exception instanceof UsernameNotFoundException) {
            result = new MyResponse<String>(ErrorCode.AUTH_FAIL, "用户名或密码不正确", null);
        } else if (exception instanceof LockedException) {
            result = new MyResponse<String>(ErrorCode.ACCOUNT_LOCKED,
                    "您的账号已被冻结，请与管理员联系", null);
        } else {
            log.error("authentication fails", exception);
            result = new MyResponse<String>(ErrorCode.UNKNOWN,
                    "系统异常，请重试或与管理员联系", null);
        }
        responseJson(httpServletResponse, result);
    }

    /* AuthenticationSuccessHandler */
    @Override
    public void onAuthenticationSuccess(final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final Authentication authentication) {
        responseJson(httpServletResponse, new MyResponse<>("登录成功",
                (Serializable) authentication.getPrincipal()));
    }

    /* LogoutSuccessHandler */
    @Override
    public void onLogoutSuccess(final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final Authentication authentication) throws ServletException {
        httpServletRequest.logout();
        httpServletRequest.getSession(true).invalidate();
        authentication.setAuthenticated(false);

        responseJson(httpServletResponse, new MyResponse<String>("退出登录成功", null));
    }

    /* AuthenticationDetailsSource */
    @Override
    public ClientInfo buildDetails(final HttpServletRequest request) {
        return new ClientInfo(getRequestIp(request), getUserAgent(request));
    }


    private static String getUserAgent(HttpServletRequest request) {
        return request.getHeader("user-agent");
    }

    private static String getRequestIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (null == ip || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("proxy-client-ip");
        }
        if (null == ip || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("wl-proxy-client-ip");
        }
        if (null == ip || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("x-real-ip");
        }
        if (null == ip || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("http-client-ip");
        }
        if (null == ip || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("http_x_forwarded_for");
        }
        if (null == ip || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (null == ip || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteHost();
        }
        return ip;
    }

    private static void responseJson(HttpServletResponse response, MyResponse<?> result) {
        try {
            response.setHeader("Content-Type", "application/json;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            final PrintWriter writer = response.getWriter();
            final String content = jsonMapper.writeValueAsString(result);
            writer.print(content);
        } catch (final Exception ex) {
            log.error("Fail to responseJson", ex);
            try {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                        "{\"timestamp\":" + System.currentTimeMillis() +
                                ",\"code\":" + ErrorCode.UNKNOWN.getCode() +
                                ",\"msg\":\"Fail to serialize MyResponse to a JSON String\"," +
                                "\"content\":null}");
            } catch (final IOException ioex) {
                log.error("Fail to sendError", ioex);
            }
        }
    }

    enum ErrorCode implements ServiceErrorCode {
        AUTH_FAIL("Authentication:fail"),
        ACCOUNT_LOCKED("Authentication:locked"),
        NEED_RE_LOGIN("Authentication:needReLogin"),
        UNKNOWN("Authentication:unknown");

        private final String code;
        ErrorCode(final String code) {
            this.code = code;
        }

        @Override
        public String getCode() {
            return this.code;
        }

        @Override
        public boolean equalsCode(final String code) {
            return this.code.equals(code);
        }
    }
}
