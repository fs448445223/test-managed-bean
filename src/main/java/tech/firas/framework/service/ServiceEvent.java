package tech.firas.framework.service;

public interface ServiceEvent {

    String getEventName();
}
