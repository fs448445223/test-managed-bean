package tech.firas.framework.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import tech.firas.framework.po.ValidationRule;

public interface ValidationRuleService {

    Map<UUID, List<ValidationRule>> getRuleMapByFieldIds(Collection<UUID> fieldIds);
}
