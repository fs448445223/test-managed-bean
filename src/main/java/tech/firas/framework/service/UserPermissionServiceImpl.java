package tech.firas.framework.service;

import javax.inject.Inject;

import org.springframework.beans.factory.InitializingBean;

import tech.firas.framework.event.EventManager;
import tech.firas.framework.po.LogModel;
import tech.firas.framework.po.ManagedBean;
import tech.firas.framework.po.PermissionBase;
import tech.firas.framework.po.User;
import tech.firas.framework.po.UserPermission;
import tech.firas.framework.repository.ManagedBeanRepository;
import tech.firas.framework.repository.UserPermissionRepository;
import tech.firas.framework.repository.UserRepository;

@lombok.extern.slf4j.Slf4j
@org.springframework.stereotype.Service
public class UserPermissionServiceImpl implements UserPermissionService, InitializingBean {

    @Override
    public void afterPropertiesSet() {
        this.eventManager.addEventListener(ManagedBeanService.Event.BEAN_CREATED.getEventName(), obj -> {
            if (!(obj instanceof LogModel)) {
                throw new IllegalArgumentException("The parameter is not a LogModel");
            }
            final LogModel log = (LogModel) obj;
            final ManagedBean managedBean = this.managedBeanRepository.findById(log.getModelId())
                    .orElseThrow(() -> new IllegalArgumentException("ManagedBean not found, id: " + log.getModelId()));
            final User user = this.userRepository.findById(log.getOperatorId())
                    .orElseThrow(() -> new IllegalStateException("User not found, id: " + log.getOperatorId()));
            this.userPermissionRepository.save(new UserPermission(
                    PermissionBase.PermissionType.OWNER, managedBean, user));
        });
        this.eventManager.addEventListener(ManagedBeanService.Event.BEAN_DELETED.getEventName(), obj -> {
            if (!(obj instanceof LogModel)) {
                throw new IllegalArgumentException("The parameter is not a LogModel");
            }
            final LogModel log = (LogModel) obj;
            this.userPermissionRepository.deleteByBeanId(log.getModelId());
        });
    }

    private EventManager<Object> eventManager;
    @Inject
    public void setEventManager(final EventManager<Object> eventManager) {
        this.eventManager = eventManager;
    }

    private UserPermissionRepository userPermissionRepository;
    @Inject
    public void setUserPermissionRepository(final UserPermissionRepository userPermissionRepository) {
        this.userPermissionRepository = userPermissionRepository;
    }

    private ManagedBeanRepository managedBeanRepository;
    @Inject
    public void setManagedBeanRepository(final ManagedBeanRepository managedBeanRepository) {
        this.managedBeanRepository = managedBeanRepository;
    }

    private UserRepository userRepository;
    @Inject
    public void setUserRepository(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
