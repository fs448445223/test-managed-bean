package tech.firas.framework.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.springframework.data.util.Pair;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import tech.firas.framework.po.PermissionBase;
import tech.firas.framework.po.User;
import tech.firas.framework.repository.RolePermissionRepository;
import tech.firas.framework.repository.UserRepository;

@lombok.extern.slf4j.Slf4j
@org.springframework.stereotype.Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        final User user = this.userRepository.getFirstByUsername(username);
        if (user == null) {
            log.info("Username not found: " + username);
            throw new UsernameNotFoundException("Username not found: " + username);
        }

        final Map<UUID, Pair<Short, PermissionBase.PermissionType>> permissionMap = user.getPermissions().stream()
                .collect(HashMap::new,
                        (map, userPermission) -> map.put( userPermission.getBean().getId(),
                                Pair.of(userPermission.getWeight(), userPermission.getPermissionType()) ),
                        Map::putAll);

        final Collection<UUID> roleIds = user.getRoles().stream().collect(ArrayList::new,
                (list, role) -> list.add(role.getId()), List::addAll);
        this.rolePermissionRepository.findByRoleIdInOrderByWeightAsc(roleIds).forEach(rolePermission -> {
            final Pair<Short, PermissionBase.PermissionType> pair = permissionMap.get(rolePermission.getBean().getId());
            if (null == pair) {
                permissionMap.put( rolePermission.getBean().getId(),
                        Pair.of(rolePermission.getWeight(), rolePermission.getPermissionType()) );
            } else if (rolePermission.getWeight() > pair.getFirst()) {
                permissionMap.put( rolePermission.getBean().getId(),
                        Pair.of(rolePermission.getWeight(), rolePermission.getPermissionType()) );
            }
        });
        final Collection<GrantedAuthority> permissions = permissionMap.entrySet().stream().collect(ArrayList::new,
                (list, entry) -> {
                    final UUID beanId = entry.getKey();
                    final PermissionBase.PermissionType permissionType = entry.getValue().getSecond();
                    if (log.isDebugEnabled()) log.debug(permissionType + ":" + beanId);
                    list.add(new SimpleGrantedAuthority(permissionType + ":" + beanId));
                }, List::addAll);
        return org.springframework.security.core.userdetails.User.builder()
                .username(username).password(user.getPassword())
                .accountLocked(user.isStatusInactive()).accountExpired(user.isStatusExpired())
                .authorities(permissions).build();
    }

    private UserRepository userRepository;
    @Inject
    public void setUserRepository(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private RolePermissionRepository rolePermissionRepository;
    @Inject
    public void setRolePermissionRepository(final RolePermissionRepository rolePermissionRepository) {
        this.rolePermissionRepository = rolePermissionRepository;
    }
}
