package tech.firas.framework.po;

@javax.persistence.Entity
@javax.persistence.Table(name = "t_user_permission",
        uniqueConstraints = { @javax.persistence.UniqueConstraint(columnNames = {"user_id", "bean_id"}) })
public class UserPermission extends PermissionBase {

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;

    public UserPermission() {}

    public UserPermission(final PermissionType permissionType, final ManagedBean managedBean, final User user) {
        this.setPermissionType(permissionType);
        this.setBean(managedBean);
        this.setUser(user);
    }

    @javax.validation.constraints.NotNull
    @javax.persistence.ManyToOne
    @javax.persistence.JoinColumn(name = "user_id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }
}
