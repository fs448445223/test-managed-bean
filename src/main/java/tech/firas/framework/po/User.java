package tech.firas.framework.po;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;

import tech.firas.framework.pojo.DefaultJsonView;

@javax.persistence.Entity
@javax.persistence.Table(name = "t_user")
public class User extends IdModel {

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(min = 4, max = 40)
    @Column(unique = true, nullable = false, length = 40)
    private String username;
    @JsonView(DefaultJsonView.class)
    public String getUsername() {
        return username;
    }
    public void setUsername(final String username) {
        this.username = username;
    }

    @NotNull
    @Column(nullable = false, length = 128)
    private String password;
    public String getPassword() {
        return password;
    }
    public void setPassword(final String password) {
        this.password = password;
    }

    @Size(max = 60)
    @Column(length = 60)
    private String nickName;
    @JsonView(DefaultJsonView.class)
    public String getNickName() {
        return nickName;
    }
    public void setNickName(final String nickName) {
        this.nickName = nickName;
    }

    @Column
    @javax.persistence.Enumerated
    private Gender gender;
    @JsonView(DefaultJsonView.class)
    public Gender getGender() {
        return gender;
    }
    public void setGender(final Gender gender) {
        this.gender = gender;
    }

    @Column
    private Long birthday;
    @JsonView(DefaultJsonView.class)
    public Long getBirthday() {
        return birthday;
    }
    public void setBirthday(final Long birthday) {
        this.birthday = birthday;
    }

    @Size(max = 512)
    @Column(length = 512)
    private String avatarUrl;
    @JsonView(DefaultJsonView.class)
    public String getAvatarUrl() {
        return avatarUrl;
    }
    public void setAvatarUrl(final String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @javax.persistence.OneToMany(targetEntity = UserPermission.class, mappedBy = "user")
    private Collection<UserPermission> permissions;
    @JsonView(ViewWithPermissions.class)
    public Collection<UserPermission> getPermissions() {
        return permissions;
    }
    public void setPermissions(final Collection<UserPermission> permissions) {
        this.permissions = permissions;
    }

    @javax.persistence.ManyToMany
    @JoinTable(name = "t_user_role", joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "role_id") })
    private Collection<Role> roles;
    @JsonView(ViewWithRoles.class)
    public Collection<Role> getRoles() {
        return roles;
    }
    public void setRoles(final Collection<Role> roles) {
        this.roles = roles;
    }

    public interface ViewWithRoles extends DefaultJsonView {}
    public interface ViewWithPermissions extends DefaultJsonView {}
    public interface ViewWithRolesAndPermissions extends ViewWithRoles, ViewWithPermissions {}

    public enum Gender {
        MALE, FEMAILE, OTHER
    }
}
