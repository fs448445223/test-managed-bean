package tech.firas.framework.po;

import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@javax.persistence.MappedSuperclass
public abstract class PermissionBase extends IdModel {

    @NotNull
    @javax.persistence.ManyToOne
    @javax.persistence.JoinColumn(name = "bean_id")
    private ManagedBean bean;

    public ManagedBean getBean() {
        return bean;
    }

    public void setBean(final ManagedBean bean) {
        this.bean = bean;
    }

    @NotNull
    @Min(0)
    @Max(30000)
    @Column
    private short weight;

    public short getWeight() {
        return weight;
    }

    public void setWeight(final short weight) {
        this.weight = weight;
    }

    @NotNull
    @Enumerated
    @Column
    private PermissionType permissionType;

    public PermissionType getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(final PermissionType permissionType) {
        this.permissionType = permissionType;
    }

    public enum PermissionType {
        VIEWER, // read only
        EDITOR, // read and write
        ADMIN, // read and write and set viewer & editor
        OWNER // read and write and set viewer & editor & admin
    }
}
