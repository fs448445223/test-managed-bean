package tech.firas.framework.po;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@javax.persistence.Entity
@javax.persistence.Table(name = "t_validation_rule")
public class ValidationRule extends IdModel {

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "field_id", nullable = false)
    @JsonIgnore
    @Getter @Setter private FieldOfBean field;

    public static final int TYPE_NULL = 1, TYPE_NOT_NULL = 2,
            TYPE_MIN = 3, TYPE_MAX = 4,
            TYPE_MATCH = 5, TYPE_NOT_MATCH = 6;

    @Min(TYPE_NULL)
    @Max(TYPE_NOT_MATCH)
    @Column(name = "validation_type", nullable = false)
    @Getter @Setter private int validationType;

    @Size(max = 2000)
    @Column(name = "validation_value", length = 2000)
    @Getter @Setter private String validationValue;

    @Size(max = 100)
    @Column(name = "error_message", length = 100)
    @Getter @Setter private String errorMessage;
}
