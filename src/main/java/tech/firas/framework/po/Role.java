package tech.firas.framework.po;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@javax.persistence.Entity
@javax.persistence.Table(name = "t_role")
public class Role extends IdModel {

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(min = 4, max = 100)
    @Column(unique = true, nullable = false, length = 100)
    private String name;

    @javax.persistence.ManyToMany
    @javax.persistence.JoinTable(name = "t_user_role", joinColumns = { @JoinColumn(name = "role_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") })
    private Collection<User> users;
    public Collection<User> getUsers() {
        return users;
    }
    public void setUsers(final Collection<User> users) {
        this.users = users;
    }
}
