package tech.firas.framework.po;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@javax.persistence.Entity
@javax.persistence.Table(name = "t_managed_bean_log")
@NoArgsConstructor
public class ManagedBeanLog extends LogModel {

    public ManagedBeanLog(final Type beanType, final UUID modelId, final String content, final boolean isCreate,
            final String operatorIp, final String userAgent, final UUID operatorId) {
        super(modelId, content, isCreate, operatorIp, userAgent, operatorId);
        this.setBeanType(beanType);
    }

    public enum Type {
        ManagedBean,
        FieldOfBean
    }

    @Getter @Setter
    @Column(name = "bean_type", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Type beanType;
}
