package tech.firas.framework.po;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@lombok.NoArgsConstructor
@MappedSuperclass
public abstract class LogModel extends IdModel {

    public LogModel(final UUID modelId, final String content, final boolean isCreate,
            final String operatorIp, final String userAgent, final UUID operatorId) {
        this.modelId = modelId;
        this.content = content;
        this.isCreate = isCreate;
        this.operatorIp = operatorIp;
        this.userAgent = userAgent;
        this.operatorId = operatorId;
    }

    @NotNull
    @Column(name = "model_id", nullable = false)
    @Getter @Setter private UUID modelId;

    @NotNull
    @Column(name = "is_create", nullable = false)
    @Getter @Setter private boolean isCreate = false;

    @Size(max = 40)
    @Column(name = "operator_ip", length = 40)
    @Getter @Setter private String operatorIp;

    @Size(max = 400)
    @Column(name = "user_agent", length = 400)
    @Getter @Setter private String userAgent;

    @Column(name = "operator_id")
    @Getter @Setter private UUID operatorId;

    @Size(max = 4000)
    @Column(nullable = false, length = 4000)
    @Getter @Setter private String content;

}
