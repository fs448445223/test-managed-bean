package tech.firas.framework.po;

@javax.persistence.Entity
@javax.persistence.Table(name = "t_role_permission",
        uniqueConstraints = { @javax.persistence.UniqueConstraint(columnNames = {"role_id", "bean_id"}) })
public class RolePermission extends PermissionBase {

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;

    public RolePermission() {}

    public RolePermission(final PermissionType permissionType, final ManagedBean managedBean, final Role role) {
        this.setPermissionType(permissionType);
        this.setBean(managedBean);
        this.setRole(role);
    }

    @javax.validation.constraints.NotNull
    @javax.persistence.ManyToOne
    @javax.persistence.JoinColumn(name = "role_id")
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }
}
