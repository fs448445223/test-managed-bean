package tech.firas.framework.po;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;

@javax.persistence.MappedSuperclass
public abstract class StatusModel extends TimeModel {

    public static final byte STATUS_DELETED = 0;
    public static final byte STATUS_NORMAL = 1;
    public static final byte STATUS_EXAMINING = 2;
    public static final byte STATUS_EDITING = 3;
    public static final byte STATUS_FROZEN = 4;
    public static final byte STATUS_UPLOADING = 5;
    public static final byte STATUS_INACTIVE = 6;
    public static final byte STATUS_USED = 7;
    public static final byte STATUS_EXPIRED = 8;

    @Column(nullable = false)
    @Getter @Setter protected byte status = STATUS_NORMAL;

    @JsonIgnore
    public boolean isStatusDeleted() {
        return STATUS_DELETED == status;
    }

    @JsonIgnore
    public boolean isStatusNormal() {
        return STATUS_NORMAL == status;
    }

    @JsonIgnore
    public boolean isStatusExamining() {
        return STATUS_EXAMINING == status;
    }

    @JsonIgnore
    public boolean isStatusEditing() {
        return STATUS_EDITING == status;
    }

    @JsonIgnore
    public boolean isStatusFrozen() {
        return STATUS_FROZEN == status;
    }

    @JsonIgnore
    public boolean isStatusUploading() {
        return STATUS_UPLOADING == status;
    }

    @JsonIgnore
    public boolean isStatusInactive() {
        return STATUS_INACTIVE == status;
    }

    @JsonIgnore
    public boolean isStatusUsed() {
        return STATUS_USED == status;
    }

    @JsonIgnore
    public boolean isStatusExpired() {
        return STATUS_EXPIRED == status;
    }


    public StatusModel statusDeleted() {
        status = STATUS_DELETED;
        return this;
    }

    public StatusModel statusNormal() {
        status = STATUS_NORMAL;
        return this;
    }

    public StatusModel statusExamining() {
        status = STATUS_EXAMINING;
        return this;
    }

    public StatusModel statusEditing() {
        status = STATUS_EDITING;
        return this;
    }

    public StatusModel statusFrozen() {
        status = STATUS_FROZEN;
        return this;
    }

    public StatusModel statusUploading() {
        status = STATUS_UPLOADING;
        return this;
    }

    public StatusModel statusInactive() {
        status = STATUS_INACTIVE;
        return this;
    }

    public StatusModel statusUsed() {
        status = STATUS_USED;
        return this;
    }

    public StatusModel statusExpired() {
        status = STATUS_EXPIRED;
        return this;
    }


    public String getStatusInfo() {
        switch (status) {
            case STATUS_DELETED:
                return "Deleted";
            case STATUS_NORMAL:
                return "Normal";
            case STATUS_EXAMINING:
                return "Examining";
            case STATUS_EDITING:
                return "Editing";
            case STATUS_FROZEN:
                return "Frozen";
            case STATUS_UPLOADING:
                return "Uploading";
            case STATUS_INACTIVE:
                return "Inactive";
            case STATUS_USED:
                return "Used";
            case STATUS_EXPIRED:
                return "Expired";
        }
        return "Undefined";
    }

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;
}
