package tech.firas.framework.po;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonView;

import tech.firas.framework.pojo.DefaultJsonView;

@javax.persistence.MappedSuperclass
public abstract class IdModel extends StatusModel {
    
    @javax.persistence.Id
    @JsonView(DefaultJsonView.class)
    @Getter @Setter protected UUID id;

    @javax.persistence.PrePersist
    @Override protected void beforeCreate() {
        super.beforeCreate();
        this.id = UUID.randomUUID();
    }

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;
}
