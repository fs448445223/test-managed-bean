package tech.firas.framework.po;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;

import tech.firas.framework.pojo.DataType;
import tech.firas.framework.pojo.DefaultJsonView;

@javax.persistence.Entity
@javax.persistence.Table(name = "t_field_of_bean")
public class FieldOfBean extends IdModel {

    public interface ViewWithBean extends DefaultJsonView {}

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "bean_id", nullable = false)
    private ManagedBean bean;
    @JsonView(ViewWithBean.class)
    public ManagedBean getBean() {
        return this.bean;
    }
    public void setBean(ManagedBean bean) {
        this.bean = bean;
    }

    @NotNull
    @Size(min = 1, max = 40)
    @Column(nullable = false, length = 40)
    private String name;
    @JsonView(DefaultJsonView.class)
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "data_type", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private DataType dataType;
    @JsonView(DefaultJsonView.class)
    public DataType getDataType() {
        return this.dataType;
    }
    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "db_column", nullable = false, length = 40)
    private String dbColumn;
    @JsonView(DefaultJsonView.class)
    public String getDbColumn() {
        return this.dbColumn;
    }
    public void setDbColumn(String dbColumn) {
        this.dbColumn = dbColumn;
    }

    @Min(1)
    @Column(name = "precision_or_length")
    private Integer precisionOrLength;
    @JsonView(DefaultJsonView.class)
    public Integer getPrecisionOrLength() {
        return this.precisionOrLength;
    }
    public void setPrecisionOrLength(Integer precisionOrLength) {
        this.precisionOrLength = precisionOrLength;
    }

    @Column(name = "date_time_pattern", length = 40)
    private String dateTimePattern;
    @JsonView(DefaultJsonView.class)
    public String getDateTimePattern() {
        return this.dateTimePattern;
    }
    public void setDateTimePattern(String dateTimePattern) {
        this.dateTimePattern = dateTimePattern;
    }

    @Column(name = "show_on_table")
    private short showOnTable = 0;
    @JsonView(DefaultJsonView.class)
    public short getShowOnTable() {
        return showOnTable;
    }
    public void setShowOnTable(final short showOnTable) {
        this.showOnTable = showOnTable;
    }

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;
}
