package tech.firas.framework.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
public abstract class TimeModel implements Serializable {

    @Column(name = "create_timestamp", nullable = false, updatable = false)
    @Getter @Setter protected long createTimestamp;

    @Column(name = "update_timestamp", nullable = false)
    @Getter @Setter protected long updateTimestamp;
    
    @javax.persistence.PrePersist
    protected void beforeCreate() {
        long now = System.currentTimeMillis();
        setCreateTimestamp(now);
        setUpdateTimestamp(now);
    }

    @javax.persistence.PreUpdate
    protected void beforeUpdate() {
        long now = System.currentTimeMillis();
        setUpdateTimestamp(now);
    }

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;
}
