package tech.firas.framework.po;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;

import tech.firas.framework.pojo.DefaultJsonView;

@javax.persistence.Entity
@javax.persistence.Table(name = "t_managed_bean")
public class ManagedBean extends IdModel {

    public interface ViewWithFields extends DefaultJsonView {}

    @NotNull
    @Size(min = 1, max = 40)
    @Pattern(regexp = "\\D+")
    @Column(unique = true, nullable = false, length = 40)
    private String name;
    @JsonView(DefaultJsonView.class)
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Size(max = 40)
    @Column(name = "db_schema", length = 40)
    private String dbSchema;
    @JsonView(DefaultJsonView.class)
    public String getDbSchema() {
        return this.dbSchema;
    }
    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "db_table", nullable = false, length = 40)
    private String dbTable;
    @JsonView(DefaultJsonView.class)
    public String getDbTable() {
        return this.dbTable;
    }
    public void setDbTable(String dbTable) {
        this.dbTable = dbTable;
    }

    @Transient
    private List<FieldOfBean> fields;
    @JsonView(ViewWithFields.class)
    public List<FieldOfBean> getFields() {
        return this.fields;
    }
    public void setFields(List<FieldOfBean> fields) {
        this.fields = fields;
    }

    @JsonView(DefaultJsonView.class)
    @Override
    public long getCreateTimestamp() {
        return super.getCreateTimestamp();
    }

    @JsonView(DefaultJsonView.class)
    @Override
    public long getUpdateTimestamp() {
        return super.getUpdateTimestamp();
    }

    @javax.persistence.Transient
    private static final long serialVersionUID = 1L;
}
